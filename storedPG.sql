-- PROCEDURE: sp_escuela_control(character varying, integer)

-- DROP PROCEDURE sp_escuela_control(character varying, integer)

CREATE OR REPLACE PROCEDURE sp_escuela_control(id_control varchar ,id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.control = id_control ;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());						 
	END LOOP;
END
$$;

-- PROCEDURE: sp_escuela_delegacion(character varying, integer)

-- DROP PROCEDURE sp_escuela_delegacion(character varying, integer);


CREATE OR REPLACE PROCEDURE sp_escuela_delegacion(id_delegacion integer, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id from schools WHERE schools.delegacion_id=id_delegacion;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());						 
	END LOOP;
END
$$



-- PROCEDURE: sp_escuela_delegacion_control(id_delegacion integer, id_control varchar, id_modulo integer)

-- DROP PROCEDURE sp_escuela_delegacion_control(id_delegacion integer, id_control varchar, id_modulo integer);


CREATE OR REPLACE PROCEDURE sp_escuela_delegacion_control(id_delegacion integer, id_control varchar, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id from schools WHERE schools.delegacion_id=id_delegacion and  schools.control = id_control ;

BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());						 
	END LOOP;
END
$$


-- PROCEDURE: sp_escuela_delegacion_nivel(id_delegacion integer, id_nivel varchar, id_modulo integer)

-- DROP PROCEDURE sp_escuela_delegacion_nivel(id_delegacion integer, id_nivel varchar, id_modulo integer)


CREATE OR REPLACE PROCEDURE sp_escuela_delegacion_nivel(id_delegacion integer, id_nivel varchar, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=id_nivel;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());						 
	END LOOP;
END
$$



-- PROCEDURE: sp_escuela_delegacion_nivel_control(id_delegacion integer, id_nivel varchar,id_control varchar, id_modulo integer)
-- DROP PROCEDURE sp_escuela_delegacion_nivel_control(id_delegacion integer, id_nivel varchar,id_control varchar, id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_delegacion_nivel_control(id_delegacion integer, id_nivel varchar,id_control varchar, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=id_nivel  and  schools.control = id_control;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$



-- PROCEDURE: sp_escuela_delegacion_nivel_servicio(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_modulo integer)
-- DROP PROCEDURE sp_escuela_delegacion_nivel_servicio(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_delegacion_nivel_servicio(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=id_nivel  and  schools.servicio_educativo=id_servicio ;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$




-- PROCEDURE: sp_escuela_delegacion_nivel_servicio_control(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_control varchar, id_modulo integer)
-- DROP PROCEDURE sp_escuela_delegacion_nivel_servicio_control(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_control varchar, id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_delegacion_nivel_servicio_control(id_delegacion integer, id_nivel varchar,id_servicio varchar, id_control varchar, id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=id_nivel  and  schools.servicio_educativo= id_servicio and  schools.control = id_control;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$



-- PROCEDURE: sp_escuela_nivel(id_nivel varchar,id_modulo integer)
-- DROP PROCEDURE sp_escuela_nivel(id_nivel varchar,id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_nivel(id_nivel varchar,id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.nivel_educativo=id_nivel;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$


-- PROCEDURE: PROCEDURE sp_escuela_nivel_control(id_nivel varchar, id_control varchar,id_modulo integer)
-- DROP PROCEDURE PROCEDURE sp_escuela_nivel_control(id_nivel varchar, id_control varchar,id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_nivel_control(id_nivel varchar, id_control varchar,id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.nivel_educativo=id_nivel and schools.control = id_control ;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$


-- PROCEDURE: PROCEDURE sp_escuela_nivel_servicio(id_nivel varchar, id_servicio varchar,id_modulo integer)
-- DROP PROCEDURE PROCEDURE sp_escuela_nivel_servicio(id_nivel varchar, id_servicio varchar,id_modulo integer)

CREATE OR REPLACE PROCEDURE sp_escuela_nivel_servicio(id_nivel varchar, id_servicio varchar,id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.nivel_educativo=id_nivel and schools.servicio_educativo=id_servicio;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$



-- PROCEDURE: `SP_ESCUELA_NIVEL_SERVICIO_CONTROL`(IN `id_nivel` VARCHAR(100), IN `id_servicio` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)
-- DROP PROCEDURE `SP_ESCUELA_NIVEL_SERVICIO_CONTROL`(IN `id_nivel` VARCHAR(100), IN `id_servicio` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)

CREATE OR REPLACE PROCEDURE sp_escuela_nivel_servicio_control(id_nivel varchar, id_servicio varchar, id_control varchar,id_modulo integer)
LANGUAGE 'plpgsql'
AS $$
DECLARE
	reg RECORD;
	cur_escuelas CURSOR FOR SELECT id FROM schools WHERE schools.nivel_educativo=id_nivel and  schools.servicio_educativo=id_servicio and  schools.control=id_control;
BEGIN
	FOR reg IN cur_escuelas LOOP
		INSERT INTO permission_schools( module_id, school_id, created_at, updated_at) VALUES(id_modulo, reg.id, now(), now());							 
	END LOOP;
END
$$