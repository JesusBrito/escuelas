<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('schools', function (Blueprint $table) { 
            $table->increments('id'); 
            $table->string('tipo_educativo'); 
            $table->string('nivel_educativo'); 
            $table->string('servicio_educativo');             
            $table->string('cct'); 
            $table->integer('id_inmueble'); 
            $table->string('turno'); 
            $table->string('nombre'); 
            $table->string('control'); 
            $table->string('direccion'); 
            $table->string('numero'); 
            $table->string('colonia'); 
            $table->integer('cp');
            $table->integer('total_alumnos'); 
            $table->decimal('lat', 10, 8); 
            $table->decimal('lng',10, 8);
            $table->integer('delegacion_id')->unsigned();
            $table->foreign('delegacion_id')->references('id')->on('delegations');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('schools');
    }
}
