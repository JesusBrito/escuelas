<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('elemento');
            $table->string('nombre');
            $table->integer('ancho');
            $table->string('etiqueta');
            $table->string('globo');
            $table->string('datos')->nullable();
            $table->string('origenDatos')->nullable();
            $table->string('observaciones');
            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_modules');
    }
}
