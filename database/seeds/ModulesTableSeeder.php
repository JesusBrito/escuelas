<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('modules')->delete();
        
        \DB::table('modules')->insert(array (
            0 => 
            array (
                'nombre' => 'Infraestructura de Servicios Básicos',
                'nombreCorto' => 'In Servicios B',
                'descripcion' => 'Infraestructura de Servicios Básicos',
                'fechaExpiracion' => '2018-11-29',
                'created_at' => '2018-11-02 00:57:07',
                'updated_at' => '2018-11-02 00:57:07',
            ),
            1 => 
            array (
                'nombre' => 'Infraestructura de Espacios Escolares y accesibilidad.',
                'nombreCorto' => 'Infra Espacios Es y acc',
                'descripcion' => 'Infraestructura de Espacios Escolares y accesibilidad.',
                'fechaExpiracion' => '2018-11-30',
                'created_at' => '2018-11-02 03:28:03',
                'updated_at' => '2018-11-02 03:28:03',
            ),
            2 => 
            array (
                'nombre' => 'Infraestructura de Seguridad e higiene',
                'nombreCorto' => 'Infra S e h',
                'descripcion' => 'Infraestructura de Seguridad e higiene',
                'fechaExpiracion' => '2018-11-30',
                'created_at' => '2018-11-02 03:34:37',
                'updated_at' => '2018-11-02 03:34:37',
            ),
            3 => 
            array (
                'nombre' => 'Prueba SP',
                'nombreCorto' => 'Prueba SP',
                'descripcion' => 'Prueba SP',
                'fechaExpiracion' => '2018-11-21',
                'created_at' => '2018-11-08 20:14:17',
                'updated_at' => '2018-11-08 20:14:17',
            ),
        ));
        
        
    }
}