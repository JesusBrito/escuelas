<?php

use Illuminate\Database\Seeder;

class DetailModulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('detail_modules')->delete();
        
        \DB::table('detail_modules')->insert(array (
            0 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E0',
                'ancho' => 6,
                'etiqueta' => 'La escuela dispone de abastecimiento continuo y suficiente de agua durante la jornada escolar ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 1,
                'created_at' => '2018-11-02 00:57:07',
                'updated_at' => '2018-11-02 00:57:07',
            ),
            1 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E1',
                'ancho' => 6,
                'etiqueta' => ' La escuela dispone de energía eléctrica durante la jornada escolar. ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 1,
                'created_at' => '2018-11-02 00:57:07',
                'updated_at' => '2018-11-02 00:57:07',
            ),
            2 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E2',
                'ancho' => 6,
                'etiqueta' => 'La escuela cuenta con algún sistema para la eliminación de aguas negras o residuales. ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 1,
                'created_at' => '2018-11-02 00:57:07',
                'updated_at' => '2018-11-02 00:57:07',
            ),
            3 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E3',
                'ancho' => 6,
                'etiqueta' => 'Los miembros de la comunidad escolar disponen de agua para beber proporcionada por la escuela.',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 1,
                'created_at' => '2018-11-02 00:57:07',
                'updated_at' => '2018-11-02 00:57:07',
            ),
            4 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E4',
                'ancho' => 6,
            'etiqueta' => 'La escuela cuenta con infraestructura sanitaria en servicio (inodoros) para estudiantes y docentes',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si|	 no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 1,
                'created_at' => '2018-11-02 03:22:37',
                'updated_at' => '2018-11-02 03:22:37',
            ),
            5 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E0',
                'ancho' => 6,
                'etiqueta' => 'La escuela cuenta con un aula para cada grupo.  ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 2,
                'created_at' => '2018-11-02 03:28:03',
                'updated_at' => '2018-11-02 03:28:03',
            ),
            6 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E1',
                'ancho' => 6,
                'etiqueta' => 'Las aulas cuentan con espacio suficiente para la cantidad y la movilidad de los estudiantes ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 2,
                'created_at' => '2018-11-02 03:28:03',
                'updated_at' => '2018-11-02 03:28:03',
            ),
            7 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E2',
                'ancho' => 6,
            'etiqueta' => 'Las aulas son ambientes físicos adecuados para el desarrollo de actividades (iluminación, audición, ventilación y temperatura). ',
                'globo' => 'Selecciona',
                'datos' => 'si	-Si	|no	-No	',
                'origenDatos' => 'Crudos',
                'observaciones' => 'Na',
                'module_id' => 2,
                'created_at' => '2018-11-02 03:28:03',
                'updated_at' => '2018-11-02 03:28:03',
            ),
            8 => 
            array (
                'elemento' => 'Combo',
                'nombre' => 'E3',
                'ancho' => 7,
                'etiqueta' => 'La escuela dispone de espacios adicionales a las aulas para el desarrollo de actividades académicas, de tutoría y propias de docentes y directivos (biblioteca, salón de usos múltiples, aula de medios, espacio para USAER, sala de maestros y dirección escol',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 2,
                    'created_at' => '2018-11-02 03:28:03',
                    'updated_at' => '2018-11-02 03:28:03',
                ),
                9 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E4',
                    'ancho' => 7,
                'etiqueta' => 'La escuela dispone de espacios para el desarrollo de actividades cívicas, deportivas, culturales y recreativas (cancha, patio o plaza cívica, áreas verdes). ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 2,
                    'created_at' => '2018-11-02 03:28:03',
                    'updated_at' => '2018-11-02 03:28:03',
                ),
                10 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E5',
                    'ancho' => 6,
                    'etiqueta' => 'La escuela cuenta con adecuaciones para el acceso y la movilidad de personas con discapacidad ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 2,
                    'created_at' => '2018-11-02 03:28:03',
                    'updated_at' => '2018-11-02 03:28:03',
                ),
                11 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E0',
                    'ancho' => 6,
                'etiqueta' => 'El plantel escolar es un inmueble seguro (muros, techos, piso, barda o cerco perimetral).   ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                12 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E1',
                    'ancho' => 6,
                    'etiqueta' => 'El plantel escolar se encuentra ubicado en una zona de bajo riesgo por afectación de desastres naturales o características del entorno.',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                13 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E2',
                    'ancho' => 6,
                    'etiqueta' => 'El inmueble escolar cuenta con condiciones que minimizan la exposición a riesgos de accidentes',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                14 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E3',
                    'ancho' => 6,
                    'etiqueta' => 'La escuela cuenta con un plan de protección civil que es conocido por la comunidad escolar.  ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                15 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E4',
                    'ancho' => 6,
                    'etiqueta' => 'La escuela cuenta con la señalización y los insumos adecuados para la prevención y la atención de contingencias  ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                16 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E5',
                    'ancho' => 6,
                    'etiqueta' => 'Las aulas y los sanitarios del plantel están limpios ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                17 => 
                array (
                    'elemento' => 'Combo',
                    'nombre' => 'E6',
                    'ancho' => 6,
                    'etiqueta' => 'La escuela es fumigada con frecuencia.  ',
                    'globo' => 'Selecciona',
                    'datos' => 'si	-Si	|no	-No	',
                    'origenDatos' => 'Crudos',
                    'observaciones' => 'Na',
                    'module_id' => 3,
                    'created_at' => '2018-11-02 03:34:38',
                    'updated_at' => '2018-11-02 03:34:38',
                ),
                18 => 
                array (
                    'elemento' => 'Texto',
                    'nombre' => 'E0',
                    'ancho' => 3,
                    'etiqueta' => 'Prueba SP',
                    'globo' => 'Prueba SP',
                    'datos' => '',
                    'origenDatos' => '',
                    'observaciones' => 'Prueba SP',
                    'module_id' => 4,
                    'created_at' => '2018-11-08 20:14:17',
                    'updated_at' => '2018-11-08 20:14:17',
                ),
            ));
        
        
    }
}