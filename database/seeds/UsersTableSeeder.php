<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'name' => 'Jesus',
                'email' => 'jesus291196@gmail.com',
                'password' => '$2y$10$hCp6tuC6Kunbe6rljaEiDOelbEZvKHsm2rfrAn.e96nDlj2IwO5Y2',
                'remember_token' => NULL,
                'created_at' => '2018-10-31 20:28:01',
                'updated_at' => '2018-10-31 20:28:01',
            ),
            1 => 
            array (
                'name' => 'José Cota',
                'email' => 'jose.cota@gmail.com',
                'password' => '$2y$10$hCp6tuC6Kunbe6rljaEiDOelbEZvKHsm2rfrAn.e96nDlj2IwO5Y2',
                'remember_token' => NULL,
                'created_at' => '2018-11-01 20:28:01',
                'updated_at' => '2018-11-01 20:28:01',
            ),
        ));
        
        
    }
}