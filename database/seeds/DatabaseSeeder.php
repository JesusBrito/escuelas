<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(DelegationsTableSeeder::class);
        $this->call(SchoolsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(DetailModulesTableSeeder::class);
        $this->call(RegistersTableSeeder::class);
        $this->call(DetailRegistersTableSeeder::class);
        $this->call(PermissionSchoolsTableSeeder::class);
    }
}
