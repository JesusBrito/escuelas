<?php

use Illuminate\Database\Seeder;

class RegistersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('registers')->delete();
        
        \DB::table('registers')->insert(array (
            0 => 
            array (
                'school_id' => 1,
                'module_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            1 => 
            array (
                'school_id' => 3,
                'module_id' => 1,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            2 => 
            array (
                'school_id' => 2,
                'module_id' => 1,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            3 => 
            array (
                'school_id' => 24,
                'module_id' => 1,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
        ));
        
        
    }
}