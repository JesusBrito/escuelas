<?php

use Illuminate\Database\Seeder;

class DetailRegistersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('detail_registers')->delete();
        
        \DB::table('detail_registers')->insert(array (
            0 => 
            array (
                'nombreCampo' => 'E0',
                'valor' => 'no',
                'register_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            1 => 
            array (
                'nombreCampo' => 'E1',
                'valor' => 'no',
                'register_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            2 => 
            array (
                'nombreCampo' => 'E2',
                'valor' => 'no',
                'register_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            3 => 
            array (
                'nombreCampo' => 'E3',
                'valor' => 'si',
                'register_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            4 => 
            array (
                'nombreCampo' => 'E4',
                'valor' => 'no',
                'register_id' => 1,
                'created_at' => '2018-11-06 16:54:06',
                'updated_at' => '2018-11-06 16:54:06',
            ),
            5 => 
            array (
                'nombreCampo' => 'E0',
                'valor' => 'si',
                'register_id' => 2,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            6 => 
            array (
                'nombreCampo' => 'E1',
                'valor' => 'no',
                'register_id' => 2,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            7 => 
            array (
                'nombreCampo' => 'E2',
                'valor' => 'si',
                'register_id' => 2,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            8 => 
            array (
                'nombreCampo' => 'E3',
                'valor' => 'si',
                'register_id' => 2,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            9 => 
            array (
                'nombreCampo' => 'E4',
                'valor' => 'si',
                'register_id' => 2,
                'created_at' => '2018-11-06 18:31:50',
                'updated_at' => '2018-11-06 18:31:50',
            ),
            10 => 
            array (
                'nombreCampo' => 'E0',
                'valor' => 'si',
                'register_id' => 3,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            11 => 
            array (
                'nombreCampo' => 'E1',
                'valor' => 'si',
                'register_id' => 3,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            12 => 
            array (
                'nombreCampo' => 'E2',
                'valor' => 'si',
                'register_id' => 3,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            13 => 
            array (
                'nombreCampo' => 'E3',
                'valor' => 'si',
                'register_id' => 3,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            14 => 
            array (
                'nombreCampo' => 'E4',
                'valor' => 'no',
                'register_id' => 3,
                'created_at' => '2018-11-06 18:33:42',
                'updated_at' => '2018-11-06 18:33:42',
            ),
            15 => 
            array (
                'nombreCampo' => 'E0',
                'valor' => 'no',
                'register_id' => 4,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
            16 => 
            array (
                'nombreCampo' => 'E1',
                'valor' => 'si',
                'register_id' => 4,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
            17 => 
            array (
                'nombreCampo' => 'E2',
                'valor' => 'si',
                'register_id' => 4,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
            18 => 
            array (
                'nombreCampo' => 'E3',
                'valor' => 'si',
                'register_id' => 4,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
            19 => 
            array (
                'nombreCampo' => 'E4',
                'valor' => 'si',
                'register_id' => 4,
                'created_at' => '2018-11-07 17:39:58',
                'updated_at' => '2018-11-07 17:39:58',
            ),
        ));
        
        
    }
}