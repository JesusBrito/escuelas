<?php

use Illuminate\Database\Seeder;

class DelegationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('delegations')->delete();
        
        \DB::table('delegations')->insert(array (
            0 => 
            array (
                'nombre' => 'Álvaro Obregón',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'nombre' => 'Azcapotzalco',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'nombre' => 'Benito Juárez',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'nombre' => 'Coyoacán',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'nombre' => 'Cuajimalpa de Morelos',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'nombre' => 'Cuauhtémoc',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'nombre' => 'Gustavo A. Madero',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'nombre' => 'Iztacalco',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'nombre' => 'Iztapalapa',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'nombre' => 'Magdalena Contreras',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'nombre' => 'Miguel Hidalgo',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'nombre' => 'Milpa Alta',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'nombre' => 'Tláhuac',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'nombre' => 'Tlalpan',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'nombre' => 'Venustiano Carranza',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'nombre' => 'Xochimilco',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}