-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbescuelas
-- ------------------------------------------------------
-- Server version   5.5.5-10.1.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'dbescuelas'
--
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_CONTROL`(IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION`(IN id_delegacion INT, IN id_modulo INT)
BEGIN

    DECLARE a INT; 
    DECLARE counter INT;
    DECLARE cantidad INT;
    DECLARE cursor1 CURSOR FOR SELECT id from schools WHERE schools.delegacion_id=id_delegacion;
    DECLARE EXIT HANDLER FOR 1329 
    BEGIN
        ROLLBACK;
    END;
    SET counter:=0;
    SET cantidad:=0;
    
    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.delegacion_id=id_delegacion);
    
    OPEN cursor1;
    REPEAT
    
        FETCH cursor1 INTO a;
        set counter := counter+1;
        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);
        
    UNTIL counter>=cantidad END REPEAT;

    CLOSE cursor1;
END ;;
DELIMITER ;

/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION_CONTROL`(IN `id_delegacion` INT, IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN



    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR SELECT id from schools WHERE schools.delegacion_id=id_delegacion and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci;

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.delegacion_id=id_delegacion and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION_NIVEL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION_NIVEL`(IN `id_delegacion` INT, IN `id_nivel` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci );

    DECLARE EXIT HANDLER FOR 1329 
    BEGIN
        ROLLBACK;
    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT count(id) FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci );

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION_NIVEL_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION_NIVEL_CONTROL`(IN `id_delegacion` INT, IN `id_nivel` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci  and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci );

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT count(id) FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci  and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION_NIVEL_SERVICIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION_NIVEL_SERVICIO`(IN `id_delegacion` INT, IN `id_nivel` VARCHAR(100) CHARSET utf8, IN `id_servicio` VARCHAR(100) CHARSET utf8, IN `id_modulo` INT)
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci);

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0; 

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo= CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_DELEGACION_NIVEL_SERVICIO_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_DELEGACION_NIVEL_SERVICIO_CONTROL`(IN `id_delegacion` INT, IN `id_nivel` VARCHAR(100), IN `id_servicio` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0; 

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.delegacion_id=id_delegacion and schools.nivel_educativo= CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci  and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_NIVEL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_NIVEL`(IN `id_nivel` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci );

    DECLARE EXIT HANDLER FOR 1329 
    BEGIN
        ROLLBACK;
    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci );

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_NIVEL_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_NIVEL_CONTROL`(IN `id_nivel` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci );

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0;

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci );

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_NIVEL_SERVICIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_NIVEL_SERVICIO`(IN `id_nivel` VARCHAR(100), IN `id_servicio` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci);

    DECLARE EXIT HANDLER FOR 1329 
    BEGIN
        ROLLBACK;
    END;

    SET counter:=0;

    SET cantidad:=0; 

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.nivel_educativo= CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_ESCUELA_NIVEL_SERVICIO_CONTROL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ESCUELA_NIVEL_SERVICIO_CONTROL`(IN `id_nivel` VARCHAR(100), IN `id_servicio` VARCHAR(100), IN `id_control` VARCHAR(100), IN `id_modulo` INT)
    NO SQL
BEGIN

    DECLARE a INT; 

    DECLARE counter INT;

    DECLARE cantidad INT;

    DECLARE cursor1 CURSOR FOR (SELECT id FROM schools WHERE schools.nivel_educativo=CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    DECLARE EXIT HANDLER FOR 1329 

    BEGIN

        ROLLBACK;

    END;

    SET counter:=0;

    SET cantidad:=0; 

    

    SET cantidad=(SELECT COUNT(schools.id) FROM schools WHERE schools.nivel_educativo= CONVERT(id_nivel USING utf8) COLLATE utf8_general_ci and  schools.servicio_educativo= CONVERT(id_servicio USING utf8)  COLLATE utf8_general_ci and  schools.control = CONVERT(id_control USING utf8)  COLLATE utf8_general_ci);

    

    OPEN cursor1;

    REPEAT

    

        FETCH cursor1 INTO a;

        set counter := counter+1;

        INSERT INTO permission_schools( module_id, school_id) VALUES(id_modulo, a);

        

    UNTIL counter>=cantidad END REPEAT;



    CLOSE cursor1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 14:31:52
