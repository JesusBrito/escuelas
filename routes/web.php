<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
//RUTAS DE EL CONTROLADOR REST
Route::resource('escuelas','EscuelaController');

Route::resource('modulos', 'ModuloController');

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'UsuariosController@mapa');
Route::delete('/eliminar-detalle-modulo/{id}', 'ModuloController@destroyDetaiModule');
Route::get('/agregar-detalles/{id}', 'ModuloController@editDetailModule');
Route::post('/guardar-detalles/{id}', 'ModuloController@updateDetailModule');
Route::post('/obtener-columnas/{nombre}', 'ModuloController@getColumns');
Route::post('/guardar-registro', 'ModuloController@storeData');
Route::post('/obtener-detalles/{id}', 'ModuloController@getModuleDetails');

Route::post('/obtener-escuelas-capas', 'ModuloController@getModuleSchools');
Route::post('/obtener-escuelas-capas-delegacion-tipo', 'ModuloController@getModuleSchoolsDelegType');

//REPORTES
Route::get('/escuelas-delegacion', 'ReportesController@escuelasDelegacion');
Route::get('/reporte/{id}', 'ReportesController@reportePorCapa');
Route::get('/generar-excel/{id}', 'ReportesController@generarExcelPorCapa');
Route::get('/obtener-respuestas/{idCapa}/{idEscuela}', 'ReportesController@respuestasPorEscuela');


//RUTAS PÚBLICAS
Route::get('/public-modulo/{id}', 'PublicController@getModule');
Route::post('/public-guardar-datos-capturados', 'PublicController@storeData');

//RUTAS JSON
Route::get('/validacion-llenado/{id_escuela}/{id_capa}', 'PublicController@validarLlenado');







