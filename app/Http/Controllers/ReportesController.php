<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use DB;
use App\Register;
use App\School;
use App\DetailRegister;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function escuelasDelegacion(){
	$escuelas = DB::select('SELECT delegations.nombre, COUNT(schools.delegacion_id) AS cantidad FROM delegations JOIN schools ON delegations.id=schools.delegacion_id GROUP BY delegations.nombre');

    	return view('reportes.escuelasPorDelegacion',['escuelas'=>$escuelas]);
    }

    public function reportePorCapa($id){
    	$capa = Module::find($id);
    	$listDetails = array();
    	$sqlString;
    	$detalles = ($capa->detailmodule())->get();
    	foreach ($detalles as $detalle){
    		$opciones=explode("|", $detalle->datos);
    		$detail= array();
    		foreach ($opciones as $opcion){
    			$opcionesDivididas=explode("-", $opcion);

    			$sqlString=" SELECT '".trim($detalle->nombre)."' AS \"nombreCampo\", '".trim($opcionesDivididas[1])."' AS etiqueta, '".trim($detalle->etiqueta)."' AS \"nombrePregunta\", count(detail_registers.valor)AS total FROM registers JOIN detail_registers ON registers.id= detail_registers.register_id WHERE registers.\"module_id\"=".$id." AND detail_registers.\"valor\"='".trim($opcionesDivididas[0])."'and detail_registers.\"nombreCampo\"='".$detalle->nombre."' ";

    			//print($sqlString);

 				$respuestas=DB::select($sqlString);
				//print_r($respuestas[0]);
    			array_push($detail, $respuestas[0]);
    			//exit();
    		}

    		array_push($listDetails, $detail);
    	}
    	//print_r($listDetails);
    	//exit();
    	return view('reportes.reportePorCapa',['respuestas'=>$listDetails,'capa'=>$capa]);
    }


    public function generarExcelPorCapa($id){
        //$capa = Module::find($id);
        //print_r($capa);
        \Excel::create('ReporteCapa', function($excel) use($id) {
            $capa = Module::find($id);
            $listDetails = array();
            $sqlString;
            $detalles = ($capa->detailmodule())->get();
            foreach ($detalles as $detalle){
                $opciones=explode("|", $detalle->datos);
                $detail= array();
                foreach ($opciones as $opcion){
                    $opcionesDivididas=explode("-", $opcion);

                    $sqlString=" SELECT '".trim($detalle->nombre)."' AS \"nombreCampo\", '".trim($opcionesDivididas[1])."' AS etiqueta, '".trim($detalle->etiqueta)."' AS \"nombrePregunta\", count(detail_registers.valor)AS total FROM registers JOIN detail_registers ON registers.id= detail_registers.register_id WHERE registers.\"module_id\"=".$id." AND detail_registers.\"valor\"='".trim($opcionesDivididas[0])."'and detail_registers.\"nombreCampo\"='".$detalle->nombre."' ";
                    $respuestas=DB::select($sqlString);
                    array_push($detail, $respuestas[0]);
                }

                array_push($listDetails, $detail);
                //print_r($listDetails);
            }

            $capa=json_decode(json_encode($capa),true);
            $listDetails=json_decode(json_encode($listDetails),true);


            $excel->sheet('Capa', function($sheet) use($listDetails, $capa){
                $labels = array();
                $values = array();
                $preguntas = array();
                $contadorColumnas="A";
                $combinadoCeldas="";
                $ant_column="A";
                $current_column="";
                $sheet->setAutoFilter(true);
                $sheet->setAutoSize(true);
                
                $sheet->setHeight(1, 30);
                foreach ($listDetails as $key => $detalle) {
                    array_push($preguntas,$detalle[0]["nombrePregunta"]);

                    foreach ($detalle as $key => $d) {
                        array_push($labels,$d["etiqueta"]);
                    }
                    foreach ($detalle as $key => $d) {
                        array_push($values,$d["total"]);
                    }
                }
                foreach ($preguntas as $key => $pregunta) {
                    $sheet->setCellValue($contadorColumnas."1",$pregunta);
                    $contadorColumnas++;
                    $contadorColumnas++;
                }
                $contadorColumnas="A";

                foreach ($labels as $key => $label) {
                    $sheet->setWidth($contadorColumnas, 35);
                    $sheet->setCellValue($contadorColumnas."2",$label);
                    $contadorColumnas++;
                }
                $contadorColumnas="A";

                foreach ($values as $key => $value) {
                    $sheet->setCellValue($contadorColumnas."3",$value);
                    $contadorColumnas++;
                }

                foreach ($listDetails as $key => $detalle) {
                    $current_column = $ant_column;

                    for($i=0; $i < count($detalle)-1; $i++) {
                        $current_column++;
                    }
                    $combinadoCeldas=$ant_column . "1:" . $current_column."1";
                    $sheet->mergeCells($combinadoCeldas);
                    $current_column++;
                    $ant_column=$current_column;
                }
                $sheet->row(1,function($row){
                    $row->setBackground('#CCCCCC');
                });
                $sheet->getStyle('A:'.$ant_column)->getAlignment()->applyFromArray(
                    array('horizontal'=>'center')
                );
                $sheet->getStyle('A:'.$ant_column)->getAlignment()->applyFromArray(
                    array('vertical'=>'center')
                );
                $sheet->getStyle('B:'.$ant_column)->getAlignment()->applyFromArray(
                    array('horizontal'=>'center')
                );
                $sheet->getStyle('B:'.$ant_column)->getAlignment()->applyFromArray(
                    array('vertical'=>'center')
                );
                $sheet->getStyle('C:'.$ant_column)->getAlignment()->applyFromArray(
                    array('horizontal'=>'center')
                );
                $sheet->getStyle('C:'.$ant_column)->getAlignment()->applyFromArray(
                    array('vertical'=>'center')
                );
                $sheet->getStyle('A1:'.$ant_column.'1')->getAlignment()->setWrapText(true);
            });
            $excel->getActiveSheet()->getStyle('A1:M1')->getfont()->setBold(true);
            $excel->getActiveSheet()->setAllBorders('thin');
        })->export('xlsx');

        return view('reportes.escuelasPorDelegacion',['escuelas'=>$escuelas]);
    }

    public function respuestasPorEscuela($idCapa, $idEscuela){
        \Excel::create('Reporte_respuestas', function($excel) use($idCapa, $idEscuela) {
            $capa = Module::find($idCapa);
            $listDetails = array();
            $sqlString;

            $sqlString='Select detail_modules.etiqueta, valor from registers join detail_registers on  registers.id = detail_registers.register_id
                                                             join modules on modules.id = registers.module_id
                                                             join detail_modules on modules.id = detail_modules.module_id
                                                             where registers.school_id ='.$idEscuela.' and registers.module_id='.$idCapa.' and detail_modules.nombre=detail_registers."nombreCampo"';


            $respuestas= DB::select($sqlString);

            $respuestas=json_decode(json_encode($respuestas),true);

            $escuela=School::find($idEscuela);

            $excel->sheet('Capa', function($sheet) use($respuestas, $escuela){
                $labels = array();
                $values = array();
                $preguntas = array();
                $contadorColumnas="A";
                $combinadoCeldas="";
                $ant_column="A";
                $current_column="";
                $sheet->setAutoFilter(true);
                $sheet->setAutoSize(true);
                $sheet->setHeight(1, 30);

                $contadorColumnas="B";

                $sheet->setCellValue("A1","Nombre de la escuela");
                $sheet->setCellValue("A2",$escuela['nombre']);
                foreach ($respuestas as $key => $respuesta) {
                    $sheet->setWidth($contadorColumnas, 35);
                    $sheet->setCellValue($contadorColumnas."1",$respuesta['etiqueta']);
                    $sheet->setCellValue($contadorColumnas."2",$respuesta['valor']);
                    $contadorColumnas++;
                }

                $sheet->getStyle('A1:ZZ1')->getAlignment()->setWrapText(true);
                $sheet->setHeight(1, 50);
            });
            $excel->getActiveSheet()->getStyle('A1:M1')->getfont()->setBold(true);
            $excel->getActiveSheet()->setAllBorders('thin');
        })->export('xlsx');
    }
}
