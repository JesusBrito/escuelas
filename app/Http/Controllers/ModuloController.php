<?php

namespace App\Http\Controllers;
set_time_limit(0);
use Illuminate\Http\Request;

use App\Module;
use App\DetailModule;
use App\Register;
use App\DetailRegister;
use App\PermissionSchool;
use Alert;
use DB;
use Response;
use App\Delegation;
use App\School;
class ModuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $modulos= Module::all();
        return view('modulos.listaModulos',['modulos'=>$modulos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
        //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
        $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
        $delegaciones = Delegation::all();
        $escuelas= School::all();
        $modulos= Module::all();
        return view('modulos.registrarModulo',['tablas'=>$tablas,'delegaciones'=>$delegaciones,"escuelas"=>$escuelas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $modulo= new Module;
        $modulo->nombre= $request->nombreModulo;
        $modulo->nombreCorto= $request->nombreCorto;
        $modulo->descripcion= $request->descripcion;
        $modulo->fechaExpiracion=date("Y-m-d", strtotime($request->fechaExpiracion));
        if($modulo->save()){
            $elemento = $request->get('elemento');
            $nombreElemento = $request->get('nombre');
            $ancho = $request->get('ancho');
            $etiqueta = $request->get('etiqueta');
            $globo = $request->get('globo');
            $datos = $request->get('datos');
            $observaciones = $request->get('observaciones');
            $origenDatos = $request->get('origenDatos');
            $delegaciones_list = $request->get('check_list_delegations');
            $nivel_list = $request->get('check_list_nivel');
            $control_list = $request->get('check_list_control');
            $servicio_list = $request->get('check_list_servicio');
            $escuelas_indiv_string = $request->get('idsIndividual');
            $check_all_options = $request->get('check_all_options');

            if($escuelas_indiv_string){
                $escuelas_indiv_array=(explode('|', $escuelas_indiv_string, strlen($escuelas_indiv_string)));
            }
            
            $cont = 0;

            while ($cont<count($elemento)) {
                $detalleModulo= new DetailModule;
                $detalleModulo->elemento=$elemento[$cont];
                $detalleModulo->nombre=$nombreElemento[$cont];
                $detalleModulo->ancho=$ancho[$cont];
                $detalleModulo->etiqueta=$etiqueta[$cont];
                $detalleModulo->globo=$globo[$cont];
                $detalleModulo->datos=($datos[$cont]);
                $detalleModulo->observaciones=$observaciones[$cont];
                $detalleModulo->module_id=$modulo->id;
                $detalleModulo->origenDatos=$origenDatos[$cont];
                $detalleModulo->save();
                $cont++;
            }
            if($check_all_options){
                try{
                    for ($j=0; $j <count($delegaciones_list) ; $j++) {
                        DB::select('CALL sp_escuela_delegacion(?,?)',array($delegaciones_list[$j], $modulo->id));
                    }
                }catch(Exception $e){}
            }else if(($nivel_list) && ($delegaciones_list) && ($servicio_list) && ($control_list)){
                for ($z=0; $z <count($control_list) ; $z++) { 
                    for ($i=0; $i <count($nivel_list) ; $i++) { 
                        for ($j=0; $j <count($delegaciones_list) ; $j++) { 
                            for ($x=0; $x <count($servicio_list) ; $x++) { 
                                try {
                                    DB::select('CALL sp_escuela_delegacion_nivel_servicio_control(?,?,?,?,?)',array($delegaciones_list[$j], $nivel_list[$i], $servicio_list[$x], $control_list[$z], $modulo->id));
                                }catch(Exception $e){}
                            }
                        }
                    } 
                }
            }else if(($nivel_list) && ($delegaciones_list) && ($servicio_list)){
                for ($i=0; $i <count($nivel_list) ; $i++) { 
                    for ($j=0; $j <count($delegaciones_list) ; $j++) { 
                        for ($x=0; $x <count($servicio_list) ; $x++) { 
                            try {
                                DB::select('CALL sp_escuela_delegacion_nivel_servicio(?,?,?,?)',array($delegaciones_list[$j], $nivel_list[$i], $servicio_list[$x], $modulo->id));
                            }catch(Exception $e){}
                        }
                    }
                }
            }else if(($delegaciones_list) && ($nivel_list) && ($control_list)){
                for ($i=0; $i <count($nivel_list) ; $i++) { 
                    for ($j=0; $j <count($delegaciones_list) ; $j++) { 
                        for ($x=0; $x <count($control_list) ; $x++) { 
                            try {
                                DB::select('CALL sp_escuela_delegacion_nivel_control(?,?,?,?)',array($delegaciones_list[$j], $nivel_list[$i], $control_list[$x], $modulo->id));
                            }catch(Exception $e){}
                        }
                    }
                }
            }else if(($nivel_list) && ($servicio_list) && ($control_list)){
                for ($i=0; $i <count($control_list) ; $i++) { 
                    for ($j=0; $j <count($nivel_list) ; $j++) { 
                        for ($x=0; $x <count($servicio_list) ; $x++) { 
                            try {
                                DB::select('CALL sp_escuela_nivel_servicio_control(?,?,?,?)',array($nivel_list[$j], $servicio_list[$x], $control_list[$i], $modulo->id));
                            }catch(Exception $e){}
                        }
                    }
                }
            }else if(($control_list) && ($delegaciones_list)){//NO
                try {

                    for ($i=0; $i<count($control_list); $i++) {
                        for ($j=0; $j<count($delegaciones_list); $j++) { 
                            DB::select('CALL sp_escuela_delegacion_control(?,?,?)',array($delegaciones_list[$j],$control_list[$i], $modulo->id));
                        }
                    }
                }catch(Exception $e){}                    
            }else if(($nivel_list) && ($delegaciones_list)){
                try {

                    for ($i=0; $i<count($nivel_list); $i++) {
                        for ($j=0; $j<count($delegaciones_list); $j++) { 
                            DB::select('CALL sp_escuela_delegacion_nivel(?,?,?)',array($delegaciones_list[$j],$nivel_list[$i], $modulo->id));
                        }
                    }
                }catch(Exception $e){}                    
            }else if(($nivel_list) && ($servicio_list)){
                try{
                    for ($i=0; $i <count($nivel_list) ; $i++) {
                        for ($x=0; $x <count($servicio_list) ; $x++) {
                            DB::select('CALL sp_escuela_nivel_servicio(?,?,?)',array($nivel_list[$i], $servicio_list[$x], $modulo->id));
                        }
                    }
                }catch(Exception $e){}
            }else if(($nivel_list) && ($control_list)){
                try{
                    for ($i=0; $i <count($nivel_list) ; $i++) {//NO
                        for ($x=0; $x <count($control_list) ; $x++) {
                            DB::select('CALL sp_escuela_nivel_control(?,?,?)',array($nivel_list[$i], $control_list[$x], $modulo->id));
                        }
                    }
                }catch(Exception $e){}
            }else if(($nivel_list)){
                try{
                    for ($i=0; $i <count($nivel_list) ; $i++) {
                        DB::select('CALL sp_escuela_nivel(?,?)',array($nivel_list[$i], $modulo->id));
                    }
                }catch(Exception $e){}
            }else if(($delegaciones_list)){
                try{
                    for ($j=0; $j <count($delegaciones_list) ; $j++) {
                        DB::select('CALL sp_escuela_delegacion(?,?)',array($delegaciones_list[$j], $modulo->id));
                    }
                }catch(Exception $e){}                
            }else if (($control_list)) {
                try{
                    for ($i=0; $i <count($control_list) ; $i++) {
                        DB::select('CALL sp_escuela_control(?,?)',array($control_list[$i], $modulo->id));
                    }
                }catch(Exception $e){}
            }

            
            if(($escuelas_indiv_string)){
                for($i=0;$i<count($escuelas_indiv_array);$i++){
                    $permissionSchool= new PermissionSchool;
                    //try{
                        $permissionSchool->module_id=$modulo->id;
                        $permissionSchool->school_id=$escuelas_indiv_array[$i];
                        $permissionSchool->save();
                    //}catch(Exception $e){}  
                } 
            }


        }
        Alert::success('La capa se registró de manera correcta', 'Registro exitoso');
        //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
        $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
        $delegaciones = Delegation::all();
        $escuelas= School::all();
        $modulos= Module::all();
        return view('modulos.listaModulos',['modulos'=>$modulos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $capa = Module::find($id);
        $detalles=$capa->detailmodule;

        for ($i=0; $i<count($detalles); $i++) { 
            if($detalles[$i]->origenDatos=="BD"){
               $detalles[$i]->datos = DB::select($detalles[$i]->datos);
            }
        }
        
        return view('modulos.visualizarCapa',['capa'=>$capa, 'detalles'=>$detalles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
        $capa = Module::find($id);
        $detalles=$capa->detailmodule;
        //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
        $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
        return view('modulos.editarModulo',['capa'=>$capa, 'detalles'=>$detalles, 'tablas'=>$tablas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id){
        
        $modulo= new Module;
        $modulo = Module::find($id);
        $modulo->nombre= $request->nombreModulo;
        $modulo->descripcion= $request->descripcion;
        $modulo->fechaExpiracion=date("Y-m-d", strtotime($request->fechaExpiracion));
        if($modulo->save()){
            $idInsert = $request->get('id');
            $elemento = $request->get('elemento');
            $nombreElemento = $request->get('nombre');
            $ancho = $request->get('ancho');
            $etiqueta = $request->get('etiqueta');
            $globo = $request->get('globo');
            $datos = $request->get('datos');
            $observaciones = $request->get('observaciones');
            $cont = 0;
            while ($cont<count($idInsert)) {
                $detalleModulo= DetailModule::find($idInsert[$cont]);
                $detalleModulo->elemento=$elemento[$cont];
                $detalleModulo->nombre=$nombreElemento[$cont];
                $detalleModulo->ancho=$ancho[$cont];
                $detalleModulo->etiqueta=$etiqueta[$cont];
                $detalleModulo->globo=$globo[$cont];
                $detalleModulo->datos=$datos[$cont];
                $detalleModulo->observaciones=$observaciones[$cont];
                $detalleModulo->module_id=$modulo->id;
                $detalleModulo->save();
                $cont++;
            }
            
            Alert::success('La capa se modificó de manera correcta', 'Registro exitoso');
            $capa = Module::find($id);
            $detalles=$capa->detailmodule;
           //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
            $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
            return view('modulos.editarModulo',['capa'=>$capa, 'detalles'=>$detalles, 'tablas'=>$tablas]);
        }else{
            Alert::error('Error', 'Error al modificar la capa');
            $capa = Module::find($id);
            $detalles=$capa->detailmodule;
            //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
            $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
            return view('modulos.editarModulo',['capa'=>$capa, 'detalles'=>$detalles, 'tablas'=>$tablas]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Module::destroy($id);     
    }



    public function destroyDetaiModule($id){
        DetailModule::destroy($id);
    }

    public function editDetailModule($id){
        //
        $capa = Module::find($id);
        $detalles=$capa->detailmodule;
        //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
        $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
        return view('modulos.agregarCampos',['capa'=>$capa, 'detalles'=>$detalles,'tablas'=>$tablas]);
    }

    public function updateDetailModule(Request $request,$id){
        $elemento = $request->get('elemento');
        $nombreElemento = $request->get('nombreElemento');
        $ancho = $request->get('ancho');
        $etiqueta = $request->get('etiqueta');
        $globo = $request->get('globo');
        $datos = $request->get('datos');
        $observaciones = $request->get('observaciones');
        $origenDatos = $request->get('origenDatos');
        $cont = 0;
        while ($cont<count($elemento)) {
            $detalleModulo= new DetailModule;
            $detalleModulo->elemento=$elemento[$cont];
            $detalleModulo->nombre=$nombreElemento[$cont];
            $detalleModulo->ancho=$ancho[$cont];
            $detalleModulo->etiqueta=$etiqueta[$cont];
            $detalleModulo->globo=$globo[$cont];
            $detalleModulo->datos=$datos[$cont];
            $detalleModulo->origenDatos=$origenDatos[$cont];
            $detalleModulo->observaciones=$observaciones[$cont];
            $detalleModulo->module_id=$id;
            $detalleModulo->save();
            $cont++;
        }
        Alert::success('Los campos se agregaron de manera correcta', 'Registro exitoso');
        $capa = Module::find($id);
        $detalles=$capa->detailmodule;
        //$tablas = DB::select('SHOW FULL TABLES FROM dbescuelas');
        $tablas = DB::select("SELECT table_name FROM information_schema.tables where table_schema = 'public'");
        return view('modulos.agregarCampos',['capa'=>$capa, 'detalles'=>$detalles,'tablas'=>$tablas]);
    }

    public function getColumns($nombre){
        //$columnas = DB::select("DESCRIBE {$nombre}");
        $columnas = DB::select("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$nombre}'");
        
        return Response::json($columnas);
    }

    public function getModuleDetails($id){
        $escuela= School::find($id);

        $details = $escuela->layers;
        return Response::json($details);
    }

    public function storeData(Request $request){

        $idCapa = $request->get('idCapa');
        $capa = Module::find($idCapa);
        $detalles=$capa->detailmodule;
        $registro = new Register;
        $registro->module_id=$idCapa;
        $registro->save();

        for ($i=0; $i < count($detalles) ; $i++) { 
            $detalleRegistro= new DetailRegister;
            $detalleRegistro->register_id=$registro->id;
            $detalleRegistro->nombreCampo= $detalles[$i]->nombre;

            if($detalles[$i]->elemento=="Date"){
               $detalleRegistro->valor = date("Y-m-d", strtotime($request->get($detalles[$i]->nombre)));
            }else{  
              $detalleRegistro->valor=($request->get($detalles[$i]->nombre));  
            }
            $detalleRegistro->save();
        }

        Alert::success('Los campos se agregaron de manera correcta', 'Registro exitoso');
        $modulos= Module::all();
        return view('modulos.listaModulos',['modulos'=>$modulos]);
    }


    public function getModuleSchools(Request $request){
        $listDetails = array();
        $listaIds = $request->get('idCapasReq');

        foreach ($listaIds["idCapas"] as $key => $id) {
            $modulo = Module::find($id["Id"]);
            $details = $modulo->schools;
            array_push($listDetails, $details);
        }

        return Response:: json($listDetails);
    }

    public function getModuleSchoolsDelegType(Request $request){
        $listDetails = array();
        $listaIds = $request->get('idCapasReq');
        $delegacion = $request->get('idDelegReq');
        $tipo = $request->get('idTipoReq');

        foreach ($listaIds["idCapas"] as $key => $id) {
            $modulo = Module::find($id["Id"]);
            $details = $modulo->schools;
            
            foreach ($details as $key => $value) {
                if(true){
                    array_push($listDetails, $value);
                }   
            }
        }
        return Response:: json($listDetails);
    }
    

}



