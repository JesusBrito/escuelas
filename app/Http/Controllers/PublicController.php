<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\DetailModule;
use App\Register;
use App\DetailRegister;
use Alert;
use Carbon\Carbon;
use Response;

class PublicController extends Controller{
    //

    public function getModule($id){
    	$capa = Module::find($id);
        $detalles=$capa->detailmodule;

        for ($i=0; $i<count($detalles); $i++) { 
            if($detalles[$i]->origenDatos=="BD"){
               $detalles[$i]->datos = DB::select($detalles[$i]->datos);
            }
        }
        if(Carbon::parse($capa->fechaExpiracion) >= Carbon::now()->toDateString()){
            return view('public.publicVisualizarCapa',['capa'=>$capa, 'detalles'=>$detalles]);
        }else{
           return view('public.vistaMensajeError'); 
        }
    }

    public function storeData(Request $request){

        $idCapa = $request->get('idCapa');
        $idEscuela= $request->get('idEscuela');
        $capa = Module::find($idCapa);
        $detalles=$capa->detailmodule;
        $registro = new Register;
        $registro->module_id=$idCapa;
        $registro->school_id=$idEscuela;
        $registro->save();

        for ($i=0; $i < count($detalles) ; $i++) { 
            $detalleRegistro= new DetailRegister;
            $detalleRegistro->register_id=$registro->id;
            $detalleRegistro->nombreCampo= $detalles[$i]->nombre;

            if($detalles[$i]->elemento=="Date"){
               $detalleRegistro->valor = date("Y-m-d", strtotime($request->get($detalles[$i]->nombre)));
            }else{  
              $detalleRegistro->valor=($request->get($detalles[$i]->nombre));  
            }
            $detalleRegistro->save();
        }
        Alert::success('Los datos se enviaron de maner adecuada', 'Registro exitoso');
        return view('public.vistaMensaje');
    }

    public function validarLlenado($id_escuela,$id_capa)
    {
        //$matchThese = ['school_id' = ''.$id_escuela.'', 'module_id' = ''.$id_capa.''];
        $valRegistro= Register::where('school_id','=',$id_escuela)
                                ->where('module_id','=', $id_capa)->get();
        return Response:: json($valRegistro);
    }
}
