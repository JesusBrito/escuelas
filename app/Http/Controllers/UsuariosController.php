<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\EloquentUserProvider;
use DB;
use Alert;
use Auth;
use App\Delegation;
use App\School;
use App\Module;
class UsuariosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function mapa(){
        $delegaciones = Delegation::all();
        $escuelas = School::all();
        $modulos = Module::all();
        return view('mapa',['delegaciones'=>$delegaciones, 'escuelas'=>$escuelas, 'modulos'=>$modulos]);
    }

     public function registra()
    {

        $rol = DB::table('rol')->get();
        $biblioteca = DB::table('biblioteca')->get();
        return view('/usuarios.registrausuarios', ['rol' => $rol, 'biblioteca' => $biblioteca]);
    }

    public function registrarusuario()
    {
        if($_POST['password']!=$_POST['password_confirmation'])
        {
            Alert::error('La contraseña y la confirmación de contraseña no coinciden', 'Error en el registro')->persistent('Cerrar');
        return back();
        }
        else{
        $biblioteca = DB::table('users')->where('email',$_POST['email'])->first();
        if(isset($biblioteca))
        {
             Alert::error('El correo electrónico ya se encuentra registrado', 'Error en el registro')->persistent('Cerrar');
        return back();
        }
        else{
        $fecha = date("Y-m-d H:i:s");
        $contra = Hash::make($_POST['password']);

          DB::table('users')->insert([
            'name' => $_POST['name'], 'apaterno' => $_POST['apaterno'], 'amaterno' => $_POST['amaterno'], 'email' => $_POST['email'], 'rol' => $_POST['rol'], 'id_biblioteca' => $_POST['biblioteca'], 'created_at' => $fecha, 'estatus' => 1, 'password' => $contra]);

          $id = DB::table('users')
                ->orderby('id', 'desc')
                ->first();

        DB::table('registro')->insert(
            ['usuario_id' => Auth::user()->id, 'accion' => 1, 'id_users' => $id->id]
        );

        Alert::success('Personal dado de alta', 'Registro exitoso')->persistent('Cerrar');
        return back();
        }
        }

    }

    public function editausuario()
    {
        $users = DB::table('users')->join('rol', 'rol.id', '=', 'users.rol')->select('users.*', 'rol.nombre as nombrerol')->get();
        $rol = DB::table('rol')->get();
        $biblioteca = DB::table('biblioteca')->get();

        return view('/usuarios.editausuarios', ['users' => $users, 'rol' => $rol, 'biblioteca' => $biblioteca]);
    }

    public function habilitausuario()
    {

        if($_GET['estatus']==1)
        {$estatus=0;}
        else{$estatus=1;}
        DB::table('users')
            ->where('id', $_GET['id'])
            ->update(['estatus' => $estatus]);

          return redirect('editausuario');
    }

    public function editarusuario()
    {
        $users = DB::table('users')->join('rol', 'rol.id', '=', 'users.rol')->select('users.*', 'rol.nombre as nombrerol')->where('users.id',$_GET['id'])->first();

        $rol = DB::table('rol')->get();
        $biblioteca = DB::table('biblioteca')->get();
        return view('/usuarios.editarusuarios', ['users' => $users, 'rol' => $rol, 'biblioteca' => $biblioteca]);

    }

    public function actualizausuario()
    {
       $fecha = date("Y-m-d H:i:s");
        DB::table('users')
            ->where('id', $_POST['id'])
            ->update(['name' => $_POST['name'],'apaterno' => $_POST['apaterno'],'amaterno' => $_POST['amaterno'],'email' => $_POST['email'],'id_biblioteca' => $_POST['biblioteca'],'rol' => $_POST['rol'], 'updated_at' => $fecha]);

        DB::table('registro')->insert(
            ['usuario_id' => Auth::user()->id, 'accion' => 2, 'id_users' => $_POST['id']]
        );

        Alert::success('Actualización de datos exitosa', 'Usuario actualizado')->persistent('Cerrar');



        return redirect('editausuario');

    }


}
