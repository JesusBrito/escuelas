<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delegation;
use App\School;

class EscuelaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        //
        $escuelas= School::all();
        $delegaciones = Delegation::all();
        return view('escuelas.visualizarEscuelas',['escuelas'=>$escuelas,'delegaciones'=>$delegaciones]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
        $delegaciones= [];
        $delegaciones = Delegation::all();
        return view('escuelas.registrarEscuela',['delegaciones'=>$delegaciones]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
        $options=[
            'tipo_educativo'=>$request->tipo,
            'nivel_educativo'=>$request->nivel,
            'servicio_educativo'=>$request->servicio,
            'delegacion_id'=>$request->delegacion,
            'cct'=>$request->cct,
            'id_inmueble'=>$request->idInmueble,
            'turno'=>$request->turno,
            'nombre'=>$request->nombre,
            'control'=>$request->control,
            'direccion'=>$request->calle,
            'numero'=>$request->numero,
            'colonia'=>$request->colonia,
            'cp'=>$request->cp,
            'total_alumnos'=>$request->totalAlumnos,
            'lat'=>$request->lat,
            'lng'=>$request->lng
        ];

        if(School::create($options)){
            return redirect('/escuelas');
        }else{
            return view('escuelas.registrarEscuela');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
        $escuela= School::find($id);
        $delegaciones = Delegation::all();
        return view('escuelas.editarEscuela',["escuela"=>$escuela, 'delegaciones'=>$delegaciones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
        $escuela = School::find($id);


        $escuela->tipo_educativo = $request->tipo;
        $escuela->nivel_educativo = $request->nivel;
        $escuela->servicio_educativo = $request->servicio;
        $escuela->delegacion_id = $request->delegacion;
        $escuela->cct = $request->cct;
        $escuela->id_inmueble = $request->idInmueble;
        $escuela->turno = $request->turno;
        $escuela->nombre = $request->nombre;
        $escuela->control = $request->control;
        $escuela->direccion = $request->calle;
        $escuela->numero = $request->numero;
        $escuela->colonia = $request->colonia;
        $escuela->cp = $request->cp;
        $escuela->total_alumnos = $request->totalAlumnos;
        $escuela->lat = $request->lat;
        $escuela->lng = $request->lng;


        if($escuela->save()){
            return redirect('/escuelas');
        }else{
            $delegaciones = Delegation::all();
            return view('escuelas.editarEscuela',["escuela"=>$escuela, 'delegaciones'=>$delegaciones]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
