<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionDetail extends Model{
    //
    public $fillable=['elemento','nombre','ancho','etiqueta','globo','datos','observaciones','module_id'];
}
