<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model{
    //
	public function detailmodule(){
		return $this-> hasMany('App\DetailModule');
	}

	public $fillable=['nombre','descripcion'];

	
	public function schools(){
		 return $this->belongsToMany('App\School', 'permission_schools')->orderBy('nombre','ASC');
	}

	public function registers(){
		 return $this->hasMany('App\Register');
	}
	
}
