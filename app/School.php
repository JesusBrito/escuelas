<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model{
    //
	public $fillable=['tipo_educativo','nivel_educativo','servicio_educativo','delegacion_id', 'cct','id_inmueble','turno','nombre', 'control','direccion','numero','colonia', 'cp','total_alumnos','lat','lng'];

	public function layers(){
		 return $this->belongsToMany('App\Module', 'permission_schools');
	}

	public function answers(){
		return $this->belongsToMany('App\Module', 'register_schools');
	}

}
