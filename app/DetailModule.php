<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailModule extends Model{
    //
    public function module(){
    	return $this-> belongsTo('App\Module');
    }
}
