<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model{
   public function schools(){
		 return $this->belongsToMany('App\School', 'register_schools');
	}
}
