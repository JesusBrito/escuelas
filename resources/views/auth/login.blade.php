<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mapa de riesgos escolares | </title>

    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

</head>

<style>
  html{
      background-color: #fff;
  }

  body{
    background-color: #fff;
  }
     @media screen and (max-width:780) and (min-width:600) {
  .logo {
  width:700px;
  max-height:50px;
  margin-top:10px;
  margin-bottom:10px;
  }
}
</style>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu col-md-12" style="height:80px">
        <nav>
            <div style="float:left" class="col-md-3">
                        <div style=""><h3>Mapa escolar</h2></div>
            </div>
            <div class="col-md-4 col-md-offset-1">
               <center>
                     <span class="image"><img src="{{ URL::asset("images") }}/logosedu.png" alt="Logo SEDU" style="max-height:50px;margin-top:13px;margin-bottom:10px;"/></span>
               </center>
           </div>
        </nav>
    </div>
</div>
<!-- /top navigation -->
  <div class="container" style="background-color:fff;">

    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top:5%">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#ff149b;color:white;"><h2>Mapa de riesgos escolares</h2></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">


                            <div class="col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Usuario" required autofocus>
                            </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                            <div class="col-md-6 col-md-offset-3">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Ingresar
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    ¿Olvidaste tu contraseña?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </section>
        </div>
    </div>
</div>
</body>
</html>
