@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }
    </style>
@endpush

@section('main_container')
@include('sweet::alert')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- page content -->
<div class="container">
  <div class="right_col" role="main">
  	<div class="container" style="margin-top:7%">
     	<div class="row">
     		<div class="col-md-12 col-sm-12 col-xs-12">
       		<div class="x_panel">
         		<div class="x_title">
         			<div class="clearfix"></div>
         		</div>
            <div class="x_content">
              <canvas id="myChart"></canvas>
            </div>
        	</div>
    		</div>
			</div>
		</div>
	</div>
</div>
@push ('scripts')
<script>
  var escuelas = {!! json_encode($escuelas) !!}

  console.log(escuelas)
  $(document).ready(function(){
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: [escuelas[0].nombre, escuelas[1].nombre, escuelas[2].nombre, escuelas[3].nombre, escuelas[4].nombre, escuelas[5].nombre, escuelas[6].nombre, escuelas[7].nombre, escuelas[8].nombre, escuelas[9].nombre, escuelas[10].nombre, escuelas[11].nombre, escuelas[12].nombre, escuelas[13].nombre, escuelas[14].nombre, escuelas[15].nombre],
            datasets: [{
                label: "Escuelas por delegación",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [escuelas[0].cantidad, escuelas[1].cantidad, escuelas[2].cantidad, escuelas[3].cantidad, escuelas[4].cantidad, escuelas[5].cantidad, escuelas[6].cantidad, escuelas[7].cantidad, escuelas[8].cantidad, escuelas[9].cantidad, escuelas[10].cantidad, escuelas[11].cantidad, escuelas[12].cantidad, escuelas[13].cantidad, escuelas[14].cantidad, escuelas[15].cantidad],
                
            }]
        },

        // Configuration options go here
        options: {}
    });
  });
</script>
@endpush
@endsection