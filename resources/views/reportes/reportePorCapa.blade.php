@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }
    </style>
@endpush

@section('main_container')
@include('sweet::alert')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- page content -->
<div class="container">
  <div class="right_col" role="main">
  	<div class="container" style="margin-top:7%">
     	<div class="row">
     		<div class="col-md-12 col-sm-12 col-xs-12">
       		<div class="x_panel">
         		<div class="x_title">
              <div class="row">
                <div class="col-md-10">
                  <h1>Reporte: {{$capa->nombre}}</h1>
                </div>
                <div class="col-md-2">
                  <a class="btn btn-success"  href="{{ url("generar-excel/{$capa->id}") }}"> <i class="fas fa-file-excel"></i></a> 
                </div>
              </div>              
         			<div class="clearfix"></div>
         		</div>
            <div id="x_content" class="x_content">

            </div>
        	</div>
    		</div>
			</div>
		</div>
	</div>
</div>
@push ('scripts')
<script>
  var respuestas = {!! json_encode($respuestas) !!}


  $(document).ready(function(){

    var ctx=[respuestas.length], chart=[respuestas.length], element=[respuestas.length], labels=[respuestas.length], datas=[respuestas.length], titles=[respuestas.length], colors= [];
    var dynamicColors = function() {
            var r = Math.floor(Math.random() * 155)+25;
            var g = Math.floor(Math.random() * 155)+25;
            var b = Math.floor(Math.random() * 155)+25;
            return "rgb(" + r + "," + g + "," + b + ")";
         };
    

    console.log(respuestas)



    $("#x_content").append(`<div class="row" id="R${respuestas.length}"></div>`)
    for(var index in respuestas){
      

      $("#R"+respuestas.length).append(`<div class="col-md-4" id="Col${index}"></div>`)

      $("#Col"+index).append(`<div class="x_panel" id="xP${index}"></div>`)

      $("#xP"+index).append(`<div class="x_title" id="xT${index}"><ul class="nav navbar-right panel_toolbox"><li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li></ul><div class="clearfix"></div></div>`)

      $("#xP"+index).append(`<div class="x_content" id="xC${index}"></div>`)

      $("#xC"+index).append(`<canvas id="C${index}"></canvas>`)
    }

    for(var index in respuestas){
      element[index]=document.getElementById("C"+index[0]+"");
    }

    for(var index in respuestas){
      ctx[index]=(element[index].getContext('2d'));
    }


    
    for(var index in respuestas){
    colors=[]
    for (var i = 0; i < respuestas[index].length; i++) {
      colors.push(dynamicColors());
      console.log(colors)
    }

    labels[index] = respuestas[index].map(function(e) {
        return e.etiqueta;
    });
    datas[index] = respuestas[index].map(function(e) {
        return e.total;
    });
    titles[index] = respuestas[index].map(function(e) {
        return e.nombrePregunta;
    });
    chart[index] = new Chart(ctx[index], {
        // The type of chart we want to create
        type: 'doughnut',

        // The data for our dataset
        data: {
            labels: labels[index],
            datasets: [{
                label: "Escuelas por delegación",
                backgroundColor: colors,
                borderColor: colors,
                data: datas[index],
                
            }]
        },

        // Configuration options go here
        options: {
          title: {
            display: true,
            text: titles[index][0],
            fontFamily: "Helvetica",
            fontSize: 12
          },
          legend: {
            position: 'left'
          }
        }
    });

    }
  });
</script>
@endpush
@endsection