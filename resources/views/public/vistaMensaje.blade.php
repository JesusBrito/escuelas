@extends('layouts.publicBlank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }

      .right_col_custom{background:#F7F7F7}.nav-md .container.body .right_col_custom{padding:10px 20px 0;margin-left:0px}
    </style>
@endpush

@section('main_container')
@include('sweet::alert')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- page content -->
<div class="container">
  <div class="right_col_custom" role="main">
     	<div class="row">
     		<div class="col-md-12 col-sm-12 col-xs-12">
       		<div class="x_panel">
         		<div class="x_title">
         			<ul class="nav navbar-right panel_toolbox">
         			</ul>
         			<div class="clearfix"></div>
         		</div>
            <div class="x_content">
              <h1>Gracias por su participación :)</h1>
            </div>
        	</div>
    		</div>
			</div>

	</div>
</div>
@push ('scripts')
<script>

</script>
@endpush
@endsection