
@push('stylesheets')
<style>
    .left_col{
        background-color: #A30909 !important ;
    }
    .side-title{
        color: #FF0000; 
        margin-left: 15px;
    }
</style>
@endpush

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="color:black">
            <a href="{{ url('/') }}" class="site_title"><i class="fas fa-graduation-cap" style="color:black"></i> <span style="color:black">Mapa escolar</span></a>
        </div>

        <div class="clearfix"></div>

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h4  style="color: #000000; margin-left: 15px;  text-decoration: underline;">Escuelas</h4>
                <ul class="nav side-menu">
                    <li>
                      <a href="{{ url('/') }}">
                          <i class="fas fa-map-marked-alt"></i>
                          Mapa
                          <span class="label label-success pull-right"></span>
                      </a>
                    </li>
                    <li><a><i class="fas fa-school"></i> Escuelas <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/escuelas/create') }}">Registrar</a></li>
                            <li><a href="{{ url('/escuelas') }}">Lista de escuelas</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <h4 class="side-title" style="color: #000000; margin-left: 15px;  text-decoration: underline;">Capas</h4>
                <ul class="nav side-menu">
                    <li><a><i class="fas fa-puzzle-piece"></i> Capas <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{url('/modulos/create')}}">Nueva capa</a></li>
                            <li><a href="{{url('/modulos')}}">Lista de capas</a></li>
                            
                        </ul>
                    </li>
                    <li>
                        <a><i class="fas fa-chart-line"></i> Reportes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{url('/escuelas-delegacion')}}">Escuelas por delegación</a>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons 

        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        -->
        <!-- /menu footer buttons -->
    </div>
</div>
