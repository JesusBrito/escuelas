<!-- top navigation -->
<style>
  @media screen and (max-width:780) and (min-width:600) {
    .logo {
    width:700px;
    max-height:50px;
    margin-bottom:10px;
    }
  }
  .navbar-brand {
    transform: translateX(-50%);
    left: 50%;
    position: absolute;
  }

  /* DEMO example styles for logo image */
  .navbar-brand {
    padding: 0px;
  }
  .navbar-brand>img {
    height: 100%;
    width: auto;
    padding: 7px 14px;
  }


</style>
<div class="" style="background-color:  #EDEDED">
  <div class="nav_menu">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand"><img src="{{ URL::asset('images') }}/logosedu.png" alt="Logo SEDU"/></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            
        </ul>
        <!--/.nav-collapse -->
      </div>
      <!--/.container-fluid -->
    </nav>
  </div>
</div>
