@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }
      textarea {
        resize: vertical;
      }
  </style>
@endpush

@section('main_container')
@include('sweet::alert')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- page content -->
<div class="container">
  	<div class="right_col" role="main">
    	<div class="container" style="margin-top:7%">
      	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
          		<div class="x_panel">
            		<div class="x_title">
              			<h2>Editar capa</h2>
              			<ul class="nav navbar-right panel_toolbox">
              			</ul>
              			<div class="clearfix"></div>
            		</div>
            		<div class="x_content">
                  {!!Form::open(['url'=>["/modulos/$capa->id"] , 'method'=>'POST'])!!}
                  {{Form::token()}}
                  <input name="_method" type="hidden" value="PUT">
		               	<div class="row">
                      <div class="col-md-4 col-md-offset-4">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreModulo" type="text" class="form-control col-md-12" name="nombreModulo" placeholder="Nombre"  autofocus value="{{$capa->nombre}}" > 
                        </div>
                        
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreCorto" type="text" class="form-control col-md-12" name="nombreCorto" placeholder="Nombre corto"  autofocus value="{{$capa->nombreCorto}}" > 
                        </div>

                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                          <textarea id="descripcion" type="text" class="form-control col-md-12" name="descripcion" placeholder="Descripción"  autofocus >{{$capa->descripcion}}</textarea>
                        </div>
                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                          <input id="fechaExpiracion" type="date" class="form-control col-md-12 " name="fechaExpiracion" placeholder="Fecha de expiración"  autofocus >
                        </div>
                      </div>
		                </div>
		                    
                    <div><h4>Elementos existentes</h4></div>
      
                      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 tabla">
                        <table class="table table-striped table-bordered table-condensed table-hover">
                          <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                            <th>Id</th>
                            <th>Elemento</th>
                            <th>Nombre</th>
                            <th>Ancho</th>
                            <th>Etiqueta</th>
                            <th>Globo</th>
                            <th>Datos</th>
                            <th>Origen</th>
                            <th>Observaciones</th>
                            <th>Opciones</th>
                          </thead>
                          <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tfoot>
                          <tbody>
                            @foreach($detalles as $detalle)
                              <tr class="selected" id="fila{{$detalle->id}}">
                                <td>
                                  <div class="input-group">
                                  <input type="hidden" class="form-control" name="id[]" value="{{$detalle->id}}">{{$detalle->id}}
                                </div>
                                </td> 
                                <td>
                                  <div class="input-group">
                                  <input type="hidden" class="form-control" name="elemento[]" value="{{$detalle->elemento}}">{{$detalle->elemento}}
                                </div>
                                </td>  
                                <td>
                                  <div class="input-group">
                                  <input type="hidden" class="form-control"  name="nombre[]" value="{{$detalle->nombre}}">{{$detalle->nombre}}
                                </div>
                                </td> 
                                <td>
                                  <div class="input-group">
                                  <input type="number" class="form-control" max="7" min="1" name="ancho[]" value="{{$detalle->ancho}}">
                                </div>
                                </td>
                                <td>
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="etiqueta[]" value="{{$detalle->etiqueta}}">
                                </div>
                                </td>
                                <td>
                                  <div class="input-group">
                                  <input type="text" class="form-control" name="globo[]" value="{{$detalle->globo}}">
                                </div>
                                </td>
                                <td>
                                  @if($detalle->origenDatos!='')
                                  <div class="input-group">
                                    <input type="text" class="form-control" readonly name="datos[]" id="rowDatos{{$detalle->id}}" value="{{$detalle->datos}}" >
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" 
                                    onclick="setDatos('{{$detalle->datos}}','{{$detalle->id}}', '{{$detalle->origenDatos}}');"><i class="fa fa-edit"></i></button>
                                  </span>
                                  </div>
                                  @else
                                    <input type="hidden" readonly name="datos[]" value="">No aplica
                                  @endif
                                </td>
                                <td>
                                  @if($detalle->origenDatos!='')
                                    <input type="hidden" name="globo[]" value="{{$detalle->origenDatos}}">{{$detalle->origenDatos}}
                                  @else
                                    <input type="hidden" readonly  value="">No aplica
                                  @endif
                                </td>
                                <td>
                                  <div class="input-group">
                                  <input class="form-control" type="text" name="observaciones[]" value="{{$detalle->observaciones}}"></div>
                                </td> 
                                <td>
                                    <button type="button" class="btn btn-warning" onclick="eliminar({{$detalle->id}});">X</button>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>      
                  <div class="col-md-2 col-md-offset-5" id="guardar">
                    <div class="form-group">
                      <input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
                      <button id="btnGuardar" class="btn btn-primary" type="submit" style="background-color:#12A3E7;color:white;border-color:#12A3E7;">Guardar</button>
                    </div>
                  </div>
                </div>
                {!!Form::close()!!} 
           	</div>
        	</div>
    		</div>
			</div>
		</div>
	</div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar opciones</h4>
          <h4 class="modal-title">Elemento: <label id="idElemento"></label></h4>
          <h4 class="modal-title">Origen: <label id="origen"></label></h4>
        </div>
        <div class="modal-body">

          <div id="seccionDB">
            <div class="row" id="wrapper-datos-tabla">
              <div class="panel panel-default">
                <div class="panel-body">
                  <br>
                  <br>
                  <div class="container">
                    <div class="col-md-10">                      
                      <div class="col-md-5 col-md-offset-1">
                        <h2>Nombre de la tabla</h2>
                        <p>Selecciona el nombre de la tabla para visualizar sus columnas</p>
                        <div id="dataDB"></div>
                      </div>
                      <div class="col-md-5 col-md-offset-1">
                        <h2>Nombre de las columnas</h2>
                        <p>Selecciona el nombre de la columna que quieres que se visualice en el combo box</p>
                        <div id="dataColumns"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="seccionCrudos">
            <div class="row">
              <div class="panel panel-default">
                <div class="panel-body">
                  <p>Opciones registradas</p>
                  <div class="col-md-12">
                    <table id="detalleOpcionesRegistradas" class="table table-striped table-bordered table-condensed table-hover ">
                      <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                        <th>Valor</th>
                        <th>Label</th>
                        <th>Opciones</th>
                      </thead>
                      <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tfoot>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row" id="wrapper-datos">
                  <p>Nuevas opciones</p>
                  <br>
                  <br>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                          <input id="valor" type="text" class="form-control col-md-12" name="valor" placeholder="Valor"  autofocus >
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                          <input id="label" type="text" class="form-control col-md-12" name="label" placeholder="Label"  autofocus >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-5 col-md-offset-5">
                          <button type="button" id="bt_add_option"  class="btn btn-primary" style="background-color:#ff149b;color:white;border-color:#ff149b;">
                            <span class="submit-text">Agregar opción</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <br>
                    <br>
                    <div class="col-md-12">
                      <table id="detalleOpciones" class="table table-striped table-bordered table-condensed table-hover ">
                        <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                          <th>Valor</th>
                          <th>Label</th>
                          <th>Opciones</th>
                        </thead>
                        <tfoot>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tfoot>
                        <tbody>
                        </tbody>
                      </table>
                    </div>            
                  </div>
                </div>
              </div>    
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                <textarea  id="datosModal" type="text" class="form-control col-md-12" name="datosModal" placeholder="Datos"  autofocus ></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="setDatosModal();">Cerrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
@push ('scripts')
<script src="{{ asset("js/global.js") }}"></script>
<script>

var contOpt=0;
var filas
var capa = {!! json_encode($capa->toArray()) !!};

  $(document).ready(function(){
    $('#fechaExpiracion').val(capa.fechaExpiracion);
    
    $('#bt_add_option').click(function(){
        agregarOpcion()
      });
  })
  function setDatos(datos, id, origenDatos){
    $("#idElemento").html(id)
    $("#origen").html(origenDatos)
    $('#datosModal').val("")
    if(origenDatos=="Crudos"){
      $("#seccionDB").hide()
      $("#seccionCrudos").show()
      $("#detalleOpcionesRegistradas > tbody").children().remove();
      $("#detalleOpciones > tbody").children().remove();
      $('#datos').val('')
      var contOpt
      var s = datos;
      var ss = s.split(",");
      for (var i in ss) {
        var conjunto=ss[i]
        var conjuntoDividido= conjunto.split("-")
        var fila='+<tr class="selected" id="filaOpt'+contOpt+'"><td><input type="hidden" name="valor[]" value="'+conjuntoDividido[0]+'">'+conjuntoDividido[0]+'</td><td><input type="hidden" name="label[]" value="'+conjuntoDividido[1]+'">'+conjuntoDividido[1]+'<td><button type="button" class="btn btn-warning" onclick="eliminarOpcion('+contOpt+');">X</button></td></tr>';
          contOpt++;
          limpiarOpt();
          $('#detalleOpcionesRegistradas').append(fila);
      }
      construirJson()
    }else{
      $('#datosModal').val($('#rowDatos'+id).val())
      $("#seccionCrudos").hide()
      $("#seccionDB").show()
      var tablasDB = {!! json_encode($tablas) !!};
      for (var i =0; i<tablasDB.length; i++) {
        if(tablasDB[i].table_name !== "migrations"){
          if(tablasDB[i].table_name !== "password_resets"){
            if(tablasDB[i].table_name != "tagging_tag_groups"){
              if(tablasDB[i].table_name != "tagging_tagged"){
                if(tablasDB[i].table_name != "tagging_tags"){
                  if(tablasDB[i].table_name != "users"){
                    var radio =`<div class="radio"><label><input type="radio" name="optNombreTabla"value="${tablasDB[i].table_name}" onclick="selectRowsTable();" >${tablasDB[i].table_name}</label></div><div class="radio">`
                    $('#dataDB').append(radio);
                  }
                }
              }
            }
          }
        }
      }   
    }
  }
  function selectRowsTable(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    var _token = $('input[name="_token"]').val();
    $('#datos').val('')
    $.ajax({
          type: "POST",
          url: urlImport+"obtener-columnas/"+nombre,
          
          data: { _token : _token },
        }).done(function(dataR) {
           $('#dataColumns').html('');
          for (var i =0; i<dataR.length; i++) {

            if(dataR[i].column_name != 'created_at'){
              if(dataR[i].column_name != 'updated_at'){
                var radio =`<div class="checkbox"><label><input type="checkbox" name="optNombreColumna"value="${dataR[i].column_name}" onclick="construirQuery()" >${dataR[i].column_name}</label></div>`
                $('#dataColumns').append(radio);
              }
            }
          } 
        })
        .error(function(data) {
          alert("ERROR AL CONSULTAR")
          //swal('¡Error!', 'No se pudo eliminar la capa', "error");
        })
  }
  function agregarOpcion(){

    valor=$("#valor").val();
    label=$("#label").val();
    var filasTab
    var celdasTab
    var validacion=true

    filasTab=($('#detalleOpciones tr'))

    for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
        celdasTab = $(filasTab[i]).find('td')
        var valorR = $(celdasTab[0])[0].innerText
        var labelR = $(celdasTab[1])[0].innerText
        if(valorR==valor){
          alert("Ya hay un elemento con ese valor")
          validacion=false
        }else if(labelR==label){
          alert("Ya hay un elemento con ese label")
          validacion=false
        }
    }


    filasTab=($('#detalleOpcionesRegistradas tr'))

    for (var i =2; i< $("#detalleOpcionesRegistradas > tbody").children().length +2; i++){
        celdasTab = $(filasTab[i]).find('td')
        var valorR = $(celdasTab[0])[0].innerText
        var labelR = $(celdasTab[1])[0].innerText
        if(valorR==valor){
          alert("Ya hay un elemento con ese valor")
          validacion=false
        }else if(labelR==label){
          alert("Ya hay un elemento con ese label")
          validacion=false
        }
    }

    
    if (validacion==true){
      if (valor!="" && label!=""){
        var fila='+<tr class="selected" id="filaOpt'+contOpt+'"><td><input type="hidden" name="valor[]" value="'+valor+'">'+valor+'</td><td><input type="hidden" name="label[]" value="'+label+'">'+label+'<td><button type="button" class="btn btn-warning" onclick="eliminarOpcion('+contOpt+');">X</button></td></tr>';
          contOpt++;
          limpiarOpt();
          $('#detalleOpciones').append(fila);
      }else{
        alert("Error al ingresar el elemento, revise los datos ingresados")
      }
      construirJson();
    }
  }

  function limpiarOpt(){
    $("#valor").val("");
    $("#label").val("");
  }

  function eliminarOpcion(index){   
    $("#filaOpt" + index).remove();
    construirJson()
  }

  function construirJson(){
    var JsonDatos=""
    $('#datosModal').val("")


    $('#detalleOpcionesRegistradas tr').each(function(){
        celdas =  $(this).find('td'); 
    });

    filas=($('#detalleOpcionesRegistradas tr'))
    for (var i =2; i< $("#detalleOpcionesRegistradas > tbody").children().length +2; i++){
      celdas = $(filas[i]).find('td')
      if($('#datosModal').val()!=""){
        var dato =", "+$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
        JsonDatos= JsonDatos+dato
        $('#datosModal').val(JsonDatos)
      }else{
        var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
        JsonDatos= JsonDatos+dato
        $('#datosModal').val(JsonDatos)
      }
    }
    $('#detalleOpciones tr').each(function(){
        celdas =  $(this).find('td'); 
    });

    filas=($('#detalleOpciones tr'))
    for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
      celdas = $(filas[i]).find('td')
      if($('#datosModal').val()!=""){
        var dato =", "+$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
        JsonDatos= JsonDatos+dato
        $('#datosModal').val(JsonDatos)
      }else{
        var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
        JsonDatos= JsonDatos+dato
        $('#datosModal').val(JsonDatos)
      }
    }
  }
  function setDatosModal() {
    var datosModal = $('#datosModal').val()
    var id = $("#idElemento").html()
    $("#rowDatos"+id).val(datosModal)
  }
  function construirQuery(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    let columnas=''
    $('input[name="optNombreColumna"]:checked').each(function() {
      columnas= columnas+$(this).val()+",' - ',"
    });
    columnas= columnas.trim()
    columnas= columnas.substring(0,columnas.length-7);
    let query=  `SELECT id, CONCAT(${columnas})  AS Label FROM ${nombre}`
    $('#datosModal').val(query)
  }
  function eliminar(index){ 
    var _token = $('input[name="_token"]').val();
      swal({
        title: '¿Estás seguro?',
        text: "¡No se podrán deshacer los cambios!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      })
      .then(function () {
        // código que elimina
        $.ajax({
          type: "delete",
          url: "https://192.168.61.23/escuelas/public/eliminar-detalle-modulo/"+index,
          //url: "http://localhost:8000/eliminar-detalle-modulo/"+index,
          data: { _token : _token },
        }).done(function(data) {
          swal('¡Eliminado!', 'El elemento ha sido eliminado.', "success");
          $("#fila" + index).remove();
          validar();
        })
        .error(function(data) {
          swal('¡Error!', 'No se pudo eliminar el elemento', "error");
        })
      },function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'No se han realizado cambios',
            'error'
          )
        }
      })
  }

  $('#fechaExpiracion').change(function() { 
    var expiracion =$("#fechaExpiracion").val();

     var today = moment().format('YYYY-MM-DD');
     
     if (expiracion<=today){
      swal('¡Error!', 'La fecha de expiracion debe ser posterior a la fecha actual', "error");
      $("#btnGuardar").attr("disabled", true);
     }else{
      $("#btnGuardar").attr("disabled", false);
     }
  })

</script>
@endpush
@endsection