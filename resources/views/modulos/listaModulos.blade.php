@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->     
@endpush

@section('main_container')
<!-- Include this after the sweet alert js file -->
@include('sweet::alert')
<!-- page content -->
<div class="container">
  <div class="right_col" role="main">
    <div class="container" style="margin-top:7%">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Lista de capas</h2>
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form>
                <table id="datatableModules"  class="table table-striped table-bordered ">
                  <thead>
                    <tr style="background-color:#ff149b;color:white;text-align: center;font-size:12px">      
                      <th>Nombre</th>     
                      <th>Descipción</th>
                      <th>Fecha de expiración</th> 
                      <th>Opciones</th>    
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($modulos as $modulo)
                      <tr style="font-size:12px" id="fila{{$modulo->id}}">    
                        <td><a  href="{{ url("modulos/{$modulo->id}") }}">{{ $modulo->nombre }}</td>     
                        <td>{{ $modulo->descripcion }}</td>
                        <td>{{ $modulo->fechaExpiracion }}</td>
                        <td style="text-align:center" width="8%">
                          <div style="display: inline-block;">
                            <a class="btn btn-info"  href="{{ url("modulos/{$modulo->id}/edit") }}"><img src="{{ URL::asset("images") }}/edit.png"" alt="Editar información"></a>
                            <a class="btn btn-success"  href="{{ url("agregar-detalles/{$modulo->id}") }}"><i class="fas fa-plus-circle"></i> </a>
                            <a class="btn btn-primary"  href="{{ url("reporte/{$modulo->id}") }}"><i class="fas fa-chart-pie"></i> </a>
                            <button type="button" class="btn btn-danger" onclick="eliminarModulo({{$modulo->id}});"><i class="fa fa-trash"></i></button>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                  </tbody> 
                </table>
                <input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
<!-- /footer content -->

<footer>
  <div class="pull-right">
  </div>
  <div class="clearfix"></div>
  
</footer>
@push ('scripts')
<script src="{{ asset("js/global.js") }}"></script>
  <script>

    $(document).ready(()=>{
      $('#datatableModules').on( 'draw', function () {
        alert( 'Procesando datos' )
      })

      $('#datatableModules').DataTable({
        language: {
          search:        "Buscar:",
          lengthMenu:    "Mostrar _MENU_ registros",
          info:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          infoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
          infoFiltered:   "(filtrado de un total de _MAX_ registros)",
          infoPostFix:    "",
          loadingRecords: "Cargando...",
          zeroRecords:    "No se encontraron resultados",
          emptyTable:     "Ningún dato disponible en esta tabla",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Último"
          },
          aria: {
              sortAscending:  ": Activar para ordenar la columna de manera ascendente",
              sortDescending: ": Activar para ordenar la columna de manera descendente"
          },
          destroy: true,
        }
      })
    })

    function eliminarModulo(index) {
      var _token = $('input[name="_token"]').val();
      swal({
        title: '¿Estás seguro?',
        text: "¡No se podrán deshacer los cambios!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      })
      .then(function () {
        // código que elimina
        $.ajax({
          type: "delete",
          //url: "http://localhost:8000/modulos/"+index,
          url: urlImport+"modulos/"+index,
          data: { _token : _token },
        }).done(function(data) {
          swal('¡Eliminado!', 'La capa ha sido eliminada.', "success");
          $("#fila" + index).remove();
          
        })
        .error(function(data) {
          swal('¡Error!', 'No se pudo eliminar la capa', "error");
        })
      },function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'No se han realizado cambios',
            'error'
          )
        }
      })
    }
  </script>
  @endpush
@endsection