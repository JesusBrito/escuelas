@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }

      .box-input{
        margin-bottom: 25px;
      }
    </style>
@endpush

@section('main_container')
@include('sweet::alert')
    <!-- page content -->
<div class="container">
  	<div class="right_col" role="main">
    	<div class="container" style="margin-top:7%">
      	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
          		<div class="x_panel">
            		<div class="x_title">
              			<h2>Crear nueva capa</h2>
              			<ul class="nav navbar-right panel_toolbox">
              			</ul>
              			<div class="clearfix"></div>
            		</div>
            		<div class="x_content">
                  <div>
                  {!!Form::open(array('url'=>'/modulos','method'=>'POST', 'id'=>'general','autocomplete'=>'off'))!!}
                  {{Form::token()}}
		               	<div class="row">
                      <div class="col-md-6 col-md-offset-3">

                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreModulo" type="text" class="form-control col-md-12 " name="nombreModulo" placeholder="Nombre"  autofocus >
                        </div>
                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreCorto" type="text" class="form-control col-md-12 " name="nombreCorto" placeholder="Nombre corto"  autofocus >
                        </div>
                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                          <textarea id="descripcion" type="text" class="form-control col-md-12 box-input" name="descripcion" placeholder="Descripción"  autofocus ></textarea>
                        </div>
                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                          <input id="fechaExpiracion" type="date" class="form-control col-md-12 " name="fechaExpiracion" placeholder="Fecha de expiración"  autofocus >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Asignar escuelas</button>
                      </div>
		                </div>
		                  <div class="row">
                        <div class="panel panel-default">
                          <div class="panel-body">

                            <div class="row">
                              <div class="col-md-6 col-md-offset-3">
                                <div class="input-group box-input">
                                    <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                                    <select id="elemento" name="elementos" class="form-control" >
                                      <option selected disabled value="0">Elemento</option>
                                      <option value="Texto">Texto</option>
                                      <option value="Combo">Combo</option>
                                      <option value="Date">Date</option>
                                    </select>
                                </div>
                                <div class="input-group box-input">
                                  <span class="input-group-addon"><i class="fas fa-font"></i></span>
                                  <input id="etiqueta" type="text" class="form-control col-md-12" name="etiqueta" placeholder="Etiqueta"  autofocus >
                                </div>

                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="col-md-6 col-md-offset-3">
                                <div class="input-group box-input">
                                    <span class="input-group-addon"><i class="fas fa-ruler"></i></span>
                                    <input id="ancho" type="number" max="7" min="1" class="form-control col-md-12 " name="ancho" placeholder="Ancho"  autofocus >
                                </div>
                                <div class="input-group box-input">
                                    <span class="input-group-addon"><i class="fas fa-info"></i></span>
                                    <input id="globo" type="text" class="form-control col-md-12" name="globo" placeholder="Globo"  autofocus >
                                </div>
                              </div>                              
                            </div>
                            
                          <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                              <div class="input-group box-input">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                <textarea id="observaciones" type="text" class="form-control col-md-12" name="observaciones" placeholder="Observaciones"  autofocus ></textarea>
                              </div>
                            </div>
                          </div>


                            <div class="row" id="wrapper-datos">
                                <div class="col-md-9 col-md-offset-4">
                                  <h2>Elige el tipo de origen de dato para el option</h2>
                                  <p>Puedes elegir dos origenes de datos: 1.- estáticos 2.- Desde una tabla en una BD</p>

                                    <div class="radio">
                                      <label><input type="radio" id="rbBb" name="rbOrigen" value="BD" onclick="limpiarTxtDatos()">Datos de BD</label>
                                    </div>
                                    <div class="radio">
                                      <label><input type="radio" id="rbEstaticos" name="rbOrigen" value="Crudos" onclick="limpiarTxtDatos()">Datos estáticos</label>
                                    </div>
                                </div>
                            </div>



                            <div class="row" id="wrapper-datos-tabla">
                                <br>
                                <br>
                                <div class="container">
                                  <div class="col-md-8 col-md-offset-2">                      
                                    <div class="col-md-4 col-md-offset-1">
                                      <h2>Nombre de la tabla</h2>
                                      <p>Selecciona el nombre de la tabla para visualizar sus columnas</p>
                                      <div id="dataDB">
                  
                                      </div>
                                    </div>
                                    <div class="col-md-4 col-md-offset-2">
                                      <h2>Nombre de las columnas</h2>
                                      <p>Selecciona el nombre de la columna que quieres que se visualice en el combo box</p>
                                      <div id="dataColumns">
                                          
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>


                            <div class="row" id="wrapper-datos-estaticos">
                              <br>
                              <br>                              
                              <div class="col-md-9 col-md-offset-3">
                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                    <input id="valor" type="text" class="form-control col-md-12" name="valor" placeholder="Valor"  autofocus >
                                  </div>
                                </div>

                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                    <input id="label" type="text" class="form-control col-md-12" name="label" placeholder="Label"  autofocus >
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-md-3">
                                    <button type="button" id="bt_add_option"  class="btn btn-primary" style="background-color:#12A3E7;color:white;border-color:#12A3E7;">
                                      <span class="submit-text">Agregar opción</span>
                                    </button>
                                  </div>
                                </div>
                              </div>


                              <div class="col-md-8 col-md-offset-2">
                                  <table id="detalleOpciones" class="table table-striped table-bordered table-condensed table-hover">
                                    <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                                      <th>Valor</th>
                                      <th>Label</th>
                                      <th>Opciones</th>
                                    </thead>
                                    <tfoot>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                    </tfoot>
                                  <tbody>
                                  </tbody>
                                </table>
                              </div>
                            </div>


                            <div class="row" id="wrapper-txt-datos">
                              <br>
                              <br>
                              <br>
                              <div class="col-md-6 col-md-offset-3">
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                  <textarea  id="datos" type="text" class="form-control col-md-12" name="datos" placeholder="Datos"  autofocus ></textarea>
                                </div>
                              </div>
                            </div>



                            <div class="row">
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-5">
                                  <button type="button" id="bt_add"  class="btn btn-primary" style="background-color:#12A3E7;color:white;border-color:#12A3E7;">
                                      <span class="submit-text">Agregar elemento </span>
                                      <span class="glyphicon glyphicon-send"></span>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>





                      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 tabla">
                        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover ">
                          <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                            <th>Elemento</th>
                            <th>Nombre</th>
                            <th>Ancho</th>
                            <th>Etiqueta</th>
                            <th>Globo</th>
                            <th>Dátos</th>
                            <th>Origen</th>
                            <th>Observaciones</th>
                            <th>Opciones</th>
                          </thead>
                          <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tfoot>
                          <tbody>
                          </tbody>
                         </table>
                        </div>
                      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
                       <div class="form-group">
                        <input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
                        <input type="submit" class="btn btn-primary" onclick="enviarFormulario()" value="Guardar">
                       </div>
                      </div>
                    </div>

                    

                      <!-- Modal -->
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                        
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Asignación de permiso sobre la capa</h4>
                              <p>Selecciona el tipo de filtrado que deseas realizar</p>
                            </div>
                            <div class="modal-body">

                              <div class="container">
                                <ul class="nav nav-tabs">
                                  <li><a data-toggle="tab" href="#menu1">Asignación general</a></li>
                                  <li><a data-toggle="tab" href="#menu2">Asignación especifica</a></li>
                                </ul>

                                <div class="tab-content">
                                  <div id="menu1" class="tab-pane fade in active">
                                    <br>
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox"  value="check_all_options" class="check_todos"/> 
                                        Marcar/Desmarcar Todos
                                      </label>
                                    </div>
                                    <div class="col-md-3">
                                      <h4>Delgaciones</h4>
                                      @foreach($delegaciones as $delegacion)
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" class="ck" name="check_list_delegations[]" value="{{ $delegacion->id }}" >{{ $delegacion->nombre }}
                                          </label>
                                        </div>
                                      @endforeach
                                    </div>
                                    
                                    


                                    <div class="col-md-3">
                                      <h4>Control</h4>
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="PÚBLICO" >Público
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="PRIVADO" >Privado
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="DIF" >DIF
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="DIF CDMX" >DIF CDMX
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="GCDMX" >GCDMX
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="COMUNITARIO" >Comunitario
                                        </label>
                                      </div>
                                      
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="FEDERAL" >Federal
                                        </label>
                                      </div>
                                      
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_control[]" class="ck" value="PARTICULAR" >Particular
                                        </label>
                                      </div>
                                    </div>









                                    <div class="col-md-3">
                                      <h4>Nivel</h4>
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="preescolar" value="PREESCOLAR" >Preescolar
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="primaria" value="PRIMARIA" >Primaria
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="secundaria" value="SECUNDARIA" >Secunadaria
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="usaer" value="USAER" >Usaer
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="cam" value="CAM" >Cam
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="inicial" value="INICIAL" >Inicial
                                        </label>
                                      </div>
                                      
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="direccion" value="DIRECCION" >Direccion
                                        </label>
                                      </div>
                                      
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_nivel[]" class="ck groupN" onchange="filtrarEscuelas(this)" id="cei" value="CEI" >Cei
                                        </label>
                                      </div>
                                    </div>


                                    <div class="col-md-3">
                                      <h4>Servicio educativo</h4>
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="primariaGeneralS" value="PRIMARIA GENERAL" >PRIMARIA GENERAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="usaerS" value="USAER" >USAER
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="preescolarGeneralS" value="PREESCOLAR GENERAL" >PREESCOLAR GENERAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="secundariaGeneralS" value="SECUNDARIA GENERAL" >SECUNDARIA GENERAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="telesecundariaS" value="TELESECUNDARIA" >TELESECUNDARIA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="secundariaTecnicaS" value="SECUNDARIA TÉCNICA" >SECUNDARIA TÉCNICA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="camS" value="CAM" >CAM
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="secundariaPTS" value="SECUNDARIA P. TRAB." >SECUNDARIA P. TRAB.
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="cendiS"  value="CENDI" >CENDI
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="educPAdultosS"  value="EDUCACIÓN PARA ADULTOS" >EDUCACIÓN PARA ADULTOS
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="primariaS"  value="PRIMARIA" >PRIMARIA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="secundariaS"  value="SECUNDARIA" >SECUNDARIA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="preescolarS" value="PREESCOLAR" >PREESCOLAR
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="inicialS" value="INICIAL" >INICIAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="ccaciS"  value="CCACI" >CCACI
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="ceiS"  value="CEI" >CEI
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="ccaiS"  value="CCAI" >CCAI
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="generalS"  value="GENERAL" >GENERAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="especialS"  value="ESPECIAL" >ESPECIAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="regionalS"  value="REGIONAL" >REGIONAL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="estanciaInfantilS"  value="ESTANCIA INFANTIL" >ESTANCIA INFANTIL
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="matematecaS"  value="MATEMATECA" >MATEMATECA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="ludotecaS"  value="LUDOTECA" >LUDOTECA
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label> 
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="almacenS"  value="ALMACEN" >ALMACEN
                                        </label>
                                      </div>

                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="check_list_servicio[]" class="ck groupSE" disabled id="cadiS"  value="CADI" >CADI
                                        </label>
                                      </div>

                                    </div>
                                  </div>
                                  <div id="menu2" class="tab-pane fade">
                                    <br>
                                    <table id="datatable"  class="table table-striped table-bordered ">
                                      <thead>
                                        <tr style="background-color:#ff149b;color:white;text-align: center;font-size:12px">      
                                          <th>Id</th>
                                          <th>Nombre</th>     
                                          <th>CCT</th>
                                          <th>Nivel</th>
                                          <th>Servicio</th>
                                          <th>Delegación</th>  
                                          <th>Asignar</th>    
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($escuelas as $escuela)
                                          <tr style="font-size:12px">
                                            <td>{{ $escuela->id }}</td>     
                                            <td>{{ $escuela->nombre }}</td>     
                                            <td>{{ $escuela->cct }}</td>
                                            <td>{{ $escuela->nivel_educativo }}</td>
                                            <td>{{ $escuela->servicio_educativo }}</td>
                                            <td>
                                              @foreach ($delegaciones as $delegacion)
                                                @if($delegacion->id==$escuela->delegacion_id)
                                                   {{$delegacion->nombre}}
                                                @endif
                                              @endforeach
                                            </td>
                                            <td style="text-align:center" width="8%">
                                              <div>
                                                <input type="checkbox" class="checkbox" onclick="asignarEscuelasIndividual(this)" name="check_list_escuelas_indiv" value="{{ $escuela->id }}" >
                                              </div>
                                            </td>
                                          </tr>
                                        @endforeach
                                      </tbody> 
                                    </table>
                                  
                                  <br>
                                  <br>
                                  </div>
                                </div>
                              </div>
                              <div id="elementosIndividuales">
                                <input hidden="true" type="text" id="idsIndividual" name="idsIndividual"> 
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                          </div>
                        </div>
                      </div>




  


                  {!!Form::close()!!} 
                </div>
              </div>
          	</div>
          </div>
    		</div>
			</div>
		</div>
	</div>
</div>

  

@push ('scripts')
<script src="{{ asset("js/global.js") }}"></script>
<script>

 $(document).ready(function(){
  $("input:submit").click(function() { return false; });
    $('#datatable').on( 'draw', function () {
        alert( 'Procesando datos' )
      })

      $('#datatable').DataTable({
        language: {
          search:        "Buscar:",
          lengthMenu:    "Mostrar _MENU_ registros",
          info:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          infoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
          infoFiltered:   "(filtrado de un total de _MAX_ registros)",
          infoPostFix:    "",
          loadingRecords: "Cargando...",
          zeroRecords:    "No se encontraron resultados",
          emptyTable:     "Ningún dato disponible en esta tabla",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Último"
          },
          aria: {
              sortAscending:  ": Activar para ordenar la columna de manera ascendente",
              sortDescending: ": Activar para ordenar la columna de manera descendente"
          },
          destroy: true,
        }
    })
    $('#bt_add').click(function(){
      agregar()
    });

    $('#bt_add_option').click(function(){
      agregarOpcion()
    });

    $('#elemento').change(function(){
      if($("#elemento option:selected").text()=='Combo'){
        $('#wrapper-datos').show();
      }else{
        $('#wrapper-datos').hide();
        $('#wrapper-datos-estaticos').hide();
        $("#wrapper-datos-tabla").hide();
        $('#wrapper-txt-datos').hide();

        //LIMPIAR TABLA
        $("#detalleOpciones > tbody").children().remove();
        $('#datos').val("")
      }
    });

    $('#rbBb').click(function(){
      $("#wrapper-datos-tabla").show();
      $('#wrapper-txt-datos').show();
      $('#wrapper-datos-estaticos').hide(); 
    });          
    
    $('#rbEstaticos').click(function(){
      $("#wrapper-datos-tabla").hide();
      $('#wrapper-datos-estaticos').show(); 
      $('#wrapper-txt-datos').show(); 
    });

    var tablasDB = {!! json_encode($tablas) !!};

    console.log(tablasDB)
    /*
    for (var i =0; i<tablasDB.length; i++) {
      if(tablasDB[i].Tables_in_dbescuelas !== "migrations"){
        if(tablasDB[i].Tables_in_dbescuelas !== "password_resets"){
          if(tablasDB[i].Tables_in_dbescuelas != "tagging_tag_groups"){
            if(tablasDB[i].Tables_in_dbescuelas != "tagging_tagged"){
              if(tablasDB[i].Tables_in_dbescuelas != "tagging_tags"){
                if(tablasDB[i].Tables_in_dbescuelas != "users"){
                  var radio =`<div class="radio"><label><input type="radio" name="optNombreTabla"value="${tablasDB[i].Tables_in_dbescuelas}" onclick="selectRowsTable();" >${tablasDB[i].Tables_in_dbescuelas}</label></div>`
                  $('#dataDB').append(radio);
                }
              }
            }
          }
        }
      }
    } 
    */
    for (var i =0; i<tablasDB.length; i++) {
      console.log(tablasDB[i])
      if(tablasDB[i].table_name !== "migrations"){
        if(tablasDB[i].table_name !== "password_resets"){
          if(tablasDB[i].table_name != "tagging_tag_groups"){
            if(tablasDB[i].table_name != "tagging_tagged"){
              if(tablasDB[i].table_name != "tagging_tags"){
                if(tablasDB[i].table_name != "users"){
                  var radio =`<div class="radio"><label><input type="radio" name="optNombreTabla"value="${tablasDB[i].table_name}" onclick="selectRowsTable();" >${tablasDB[i].table_name}</label></div>`
                  $('#dataDB').append(radio);
                }
              }
            }
          }
        }
      }
    }
  });

  var cont=0;
  var contOpt=0;
  var filas
  var idsIndividual=""
  $("#guardar").hide();
  $('#wrapper-datos').hide();
  $("#wrapper-datos-tabla").hide();
  $('#wrapper-datos-estaticos').hide();
  $('#wrapper-txt-datos').hide();

  function asignarEscuelasIndividual(elemento){
    //console.log(elemento.value);
    if(idsIndividual==""){
      idsIndividual = (elemento.value)
    }else{
      idsIndividual = idsIndividual+("|"+elemento.value)
    }
    $("#idsIndividual").val(idsIndividual)
  }


  function selectRowsTable(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    var _token = $('input[name="_token"]').val();
    $('#datos').val('')
    $.ajax({
          type: "POST",
          url: urlImport+"obtener-columnas/"+nombre,          
        data: { _token : _token }
        }).done(function(dataR) {
           $('#dataColumns').html('');
          for (var i =0; i<dataR.length; i++) {

            if(dataR[i].column_name != 'created_at'){
              if(dataR[i].column_name != 'updated_at'){
                var radio =`<div class="checkbox"><label><input type="checkbox" name="optNombreColumna"value="${dataR[i].column_name}" onclick="construirQuery()" > ${dataR[i].column_name}</label></div>`
                $('#dataColumns').append(radio);
              }
            }
          } 
        })
        .error(function(data) {
          alert("ERROR AL CONSULTAR")
          //swal('¡Error!', 'No se pudo eliminar la capa', "error");
        })
  }
 function agregar(){
    elemento=$("#elemento option:selected").text();
    nombre="E"+cont
    nombre= nombre.replace(/\s/g,"_");
    ancho=$("#ancho").val();
    etiqueta=$("#etiqueta").val();
    globo=$("#globo").val();
    observaciones=$("#observaciones").val();

    var filasTab
    var celdasTab
    var validacion=true
    filasTab=($('#detalles tr'))
    if(ancho<=7){
      for (var i =2; i< $("#detalles > tbody").children().length +2; i++){
          celdasTab = $(filasTab[i]).find('td')
          var dato =$(celdasTab[1])[0].innerText
          if(dato==nombre){
            alert("Ya hay un elemento con ese nombre")
            validacion=false
          }
      }
      
      if(($("#elemento option:selected").text()=='Combo') && validacion==true){
        var datos=$("#datos").val();
        let origenDatos = $('input[name="rbOrigen"]:checked').val();

        if (elemento!="" && nombre!="" && ancho!="" && etiqueta!="" && globo!=""&& datos!=""){
          var fila='+<tr class="selected" id="fila'+cont+'"><td><input type="hidden" name="elemento[]" value="'+elemento+'">'+elemento+'</td> <td><input type="hidden" name="nombre[]" value="'+nombre+'">'+nombre+'</td> <td><input type="hidden" name="ancho[]" value="'+ancho+'">'+ancho+'</td><td><input type="hidden" name="etiqueta[]" value="'+etiqueta+'">'+etiqueta+'</td><td><input type="hidden" name="globo[]" value="'+globo+'">'+globo+'</td><td><input type="hidden" name="datos[]" value="'+datos+'">'+datos+'</td><td><input type="hidden" name="origenDatos[]" value="'+origenDatos+'">'+origenDatos+'</td><td><input type="hidden" name="observaciones[]" value="'+observaciones+'">'+observaciones+'</td> <td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td></tr>';
          cont++;
          limpiar();
          $('#detalles').append(fila);
          validar();
        }else{
          alert("Error al ingresar el elemento, revise los datos ingresados")
        }   
      }else if( validacion==true){
        if (elemento!="" && nombre!="" && ancho!="" && etiqueta!="" && globo!=""){

          var fila='+<tr class="selected" id="fila'+cont+'"><td><input type="hidden" name="elemento[]" value="'+elemento+'">'+elemento+'</td> <td><input type="hidden" name="nombre[]" value="'+nombre+'">'+nombre+'</td> <td><input type="hidden" name="ancho[]" value="'+ancho+'">'+ancho+'</td><td><input type="hidden" name="etiqueta[]" value="'+etiqueta+'">'+etiqueta+'</td><td><input type="hidden" name="globo[]" value="'+globo+'">'+globo+'</td><td><input type="hidden" name="datos[]" value="">No aplica</td> <td><input type="hidden" name="origenDatos[]" value="">No aplica</td><td><input type="hidden" name="observaciones[]" value="'+observaciones+'">'+observaciones+'</td> <td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td></tr>';
          cont++;
          limpiar();
          $('#detalles').append(fila);
          validar();
        }else{
          alert("Error al ingresar el elemento, revise los datos ingresados")
        }  
      }
    }else{
      alert("El ancho debe ser menor o igual a 7")
    }
  }
  

  function agregarOpcion(){
    valor=$("#valor").val().trim();
    label=$("#label").val().trim();


    var filasTab
    var celdasTab
    var validacion=true
    filasTab=($('#detalleOpciones tr'))

    for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
        celdasTab = $(filasTab[i]).find('td')
        var valorR = $(celdasTab[0])[0].innerText
        var labelR = $(celdasTab[1])[0].innerText
        if(valorR==valor){
          alert("Ya hay un elemento con ese valor")
          validacion=false
        }else if(labelR==label){
          alert("Ya hay un elemento con ese label")
          validacion=false
        }
    }
    if (validacion==true){
      if (valor!="" && label!=""){
        var fila='+<tr class="selected" id="filaOpt'+contOpt+'"><td><input type="hidden" name="valor[]" value="'+valor+'">'+valor+'</td><td><input type="hidden" name="label[]" value="'+label+'">'+label+'<td><button type="button" class="btn btn-warning" onclick="eliminarOpcion('+contOpt+');">X</button></td></tr>';
          contOpt++;
          limpiarOpt();
          $('#detalleOpciones').append(fila);
          validar();
      }else{
        alert("Error al ingresar el elemento, revise los datos ingresados")
      }
      construirJson();
    }
  }

 function limpiar(){
    $("#nombreElemento").val("");
    $("#ancho").val("");
    $("#etiqueta").val("");
    $("#globo").val("");
    $("#datos").val("");
    $("#observaciones").val("");
    $("#detalleOpciones > tbody").children().remove();
  }

  function limpiarTxtDatos(){
     $('#datos').val("")
  }
  function limpiarOpt(){
    $("#valor").val("");
    $("#label").val("");
  }

 function eliminar(index){   
    $("#fila" + index).remove();
    validar();
 }

 function eliminarOpcion(index){   
    $("#filaOpt" + index).remove();
    construirJson()
 }

 function construirJson(){
  var JsonDatos=''
  $('#detalleOpciones tr').each(function(){
      celdas =  $(this).find('td'); 
  });

  filas=($('#detalleOpciones tr'))
  for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
    celdas = $(filas[i]).find('td')
    if(i==$("#detalleOpciones > tbody").children().length +1){
      var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
      JsonDatos= JsonDatos+dato
      $('#datos').val(JsonDatos)
    }else{
      var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText+"|"
      JsonDatos= JsonDatos+dato
      $('#datos').val(JsonDatos)
    }
  }
 }
  function construirQuery(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    let columnas=''
    $('input[name="optNombreColumna"]:checked').each(function() {
      columnas= columnas+$(this).val()+",' - ',"
    });
    columnas= columnas.trim()
    columnas= columnas.substring(0,columnas.length-7);
    let query=  `SELECT id, CONCAT(${columnas})  AS Label FROM ${nombre}`
    $('#datos').val(query)
  }

  function filtrarEscuelas(elemento) {
    $("input.groupSE").prop("disabled", true);
    $("input.groupSE").prop("checked", false);

    $("input.groupN:checked").each(function() {
        //console.log($(this).val())
        var checkSeleccionado = $(this).val()
        switch(checkSeleccionado) {
            case "PREESCOLAR":
                  $("#preescolarGeneralS").prop("disabled", false);
                  $("#cendiS").prop("disabled", false);
                  $("#preescolarS").prop("disabled", false);
                  $("#inicialS").prop("disabled", false);
                  $("#camS").prop("disabled", false);
                break;
            case "PRIMARIA":
                  $("#primariaGeneralS").prop("disabled", false);
                  $("#educPAdultosS").prop("disabled", false);
                  $("#primariaS").prop("disabled", false);
                  $("#preescolarS").prop("disabled", false);
                break;
            case "SECUNDARIA":
                  $("#secundariaGeneralS").prop("disabled", false);
                  $("#telesecundariaS").prop("disabled", false);
                  $("#secundariaTecnicaS").prop("disabled", false);
                  $("#secundariaPTS").prop("disabled", false);
                  $("#educPAdultosS").prop("disabled", false);
                  $("#secundariaS").prop("disabled", false);
                  $("#almacenS").prop("disabled", false);
                break;
            case "USAER":
                  $("#usaerS").prop("disabled", false);
                break;
            case "CAM":
                  $("#camS").prop("disabled", false);
                  $("#ceiS").prop("disabled", false);
                break;
            case "INICIAL":
                  $("#inicialS").prop("disabled", false);
                  $("#ccaciS").prop("disabled", false);
                  $("#estanciaInfantilS").prop("disabled", false);
                  $("#preescolarS").prop("disabled", false);
                  $("#cendiS").prop("disabled", false);
                  $("#matematecaS").prop("disabled", false);
                  $("#ludotecaS").prop("disabled", false);
                  $("#cadiS").prop("disabled", false);
                  $("#ceiS").prop("disabled", false);
                  $("#ccaiS").prop("disabled", false);
                break;
            case "DIRECCION":
                  $("#generalS").prop("disabled", false);
                  $("#especialS").prop("disabled", false);
                  $("#regionalS").prop("disabled", false);
                break;
            case "CEI":
                  $("#ceiS").prop("disabled", false);
                break;
            default:
        }
    });
    //console.log(elemento)
  }
 function validar(){
  var nFilas = $("#detalles > tbody").children().length;
  if (nFilas>0){
    $("#guardar").show()
  }else{
    $("#guardar").hide()
  }
 }
 function enviarFormulario() {
  var contador=0
  var texto= $("#idsIndividual").val()
  $("input[class=ck]").each(function(){
      if($(this).is(":checked"))
        contador++;
  });
  if (contador>0 || texto.length>0){
    $("#general").submit();
  }else{
    swal('¡Error!', 'Debe asignar los permisos sobre la capa', "error");
  }
  /*
 
  */
 }
  $(".check_todos").change(function(event){
    $(".ck").prop('checked', $(this).prop("checked"));
  });

  $('#fechaExpiracion').change(function() { 
    var expiracion =$("#fechaExpiracion").val();

     var today = moment().format('YYYY-MM-DD');
     
     if (expiracion<=today){
      swal('¡Error!', 'La fecha de expiracion debe ser posterior a la fecha actual', "error");
      $("#bt_add").attr("disabled", true);
     }else{
      $("#bt_add").attr("disabled", false);
     }
  });

</script>
@endpush
@endsection