
@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }
    </style>
@endpush

@section('main_container')
@include('sweet::alert')

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- page content -->
<div class="container">
  <div class="right_col" role="main">
  	<div class="container" style="margin-top:7%">
     	<div class="row">
     		<div class="col-md-12 col-sm-12 col-xs-12">
       		<div class="x_panel">
         		<div class="x_title">
         			<h2>Capa {{$capa->nombre}}</h2>
         			<ul class="nav navbar-right panel_toolbox">
         			</ul>
         			<div class="clearfix"></div>
         		</div>
            <div class="x_content">
              {!!Form::open(['url'=>["/guardar-registro"], 'class'=>'form-horizontal',  'method'=>'POST'])!!}
              {{Form::token()}}
                <input type="hidden" name="idCapa" value="{{$capa->id}}">
                <div id="formulario"></div>

              {!!Form::close()!!} 
            </div>
        	</div>
    		</div>
			</div>
		</div>
	</div>
</div>
@push ('scripts')
<script>

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
  var detallesCapa = {!! json_encode($detalles->toArray()) !!};
  
  var wrapper = $("#formulario")
  detallesCapa.forEach((item)=>{
    var columnasRestantes="col-md-offset-4"
    if(item.elemento=="Texto"){
      var ancho= "col-md-"+item.ancho+"";
      $(wrapper).append('<div class="form-group" data-toggle="tooltip" title="'+item.globo+'" "><label class="control-label col-md-3" for="'+item.nombre+'">'+item.etiqueta+'</label>' +  '<div id="'+item.nombre+'"> <input class="form-control" required type="text" placeholder="'+item.globo+'"name="'+item.nombre+'"/></div></div>');

      $("#"+item.nombre+"").addClass(""+ancho+"");
      //$("#"+item.nombre+"").addClass(""+columnasRestantes+"");

    }else if(item.elemento=="Combo"){
      var opciones= '';
      if(item.origenDatos=="Crudos"){
        var s = item.datos;
        var ss = s.split("|");
        
        for (var i in ss) {
          var conjunto=ss[i]
          var conjuntoDividido= conjunto.split("-")
          opciones= opciones+'<option value="'+conjuntoDividido[0].trim()+'">'+conjuntoDividido[1].trim()+'</option>';
        }
      }else{
        for (let i in item.datos) {
          opciones= opciones+'<option value="'+item.datos[i].id+'">'+item.datos[i].Label+'</option>';
        }
      }
      var ancho= "col-md-"+item.ancho+"";

      $(wrapper).append('<div class="form-group" data-toggle="tooltip"  title="'+item.globo+'" "> <label class="control-label col-md-3" for="'+item.nombre+'">'+item.etiqueta+'</label> <div id="'+item.nombre+'"> <select name="'+item.nombre+'" class="form-control" required> <option selected disabled value="0">'+item.etiqueta+'</option>'+opciones+'</select> </div> </div>');
         
      $("#"+item.nombre+"").addClass(""+ancho+"");
      //$("#"+item.nombre+"").addClass(""+columnasRestantes+"");

    }else{
      var ancho= "col-md-"+item.ancho+"";

      $(wrapper).append('<div class="form-group" data-toggle="tooltip"  title="'+item.globo+'" > <label class="control-label col-md-3" for="'+item.nombre+'">'+item.etiqueta+'</label> <div id="'+item.nombre+'"> <input type="date" class="form-control" required name="'+item.nombre+'"/> <span class="input-group"> </span></div></div>');

       $("#"+item.nombre+"").addClass(""+ancho+"");
       //$("#"+item.nombre+"").addClass(""+columnasRestantes+"");
    }

  })

  $(wrapper).append('<br/> <br/> <br/><div class="col-md-6 col-md-offset-5"> <button type="submit" id="bt_add"  class="btn btn-primary" style="background-color:#ff149b;color:white;border-color:#ff149b;"> <span class="submit-text">Envíar </span><span class="glyphicon glyphicon-send"></span></button></div>')
</script>
@endpush
@endsection