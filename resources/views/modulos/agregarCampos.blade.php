@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			 padding:6px;
		  }
      .tabla{
        margin-top: 15px;
      }
      .box-input{
        margin-bottom: 25px;
      }
    </style>
@endpush

@section('main_container')
@include('sweet::alert')


    <!-- page content -->
<div class="container">
  <div class="right_col" role="main">
  	<div class="container" style="margin-top:7%">
     	<div class="row">
     		<div class="col-md-12 col-sm-12 col-xs-12">
       		<div class="x_panel">
         		<div class="x_title">
         			<h2>Agregar elementos a la capa</h2>
           			<ul class="nav navbar-right panel_toolbox">
           			</ul>
           			<div class="clearfix"></div>
            </div>
            <div class="x_content">
              {!!Form::open(['url'=>["/guardar-detalles/$capa->id"] , 'method'=>'POST'])!!}
              {{Form::token()}}

                <div class="row">
                  <div class="panel panel-default">
                    <div class="panel-body">

                      <div class="col-md-4 col-md-offset-4">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreModulo" type="text" readonly class="form-control col-md-12" name="nombreModulo" placeholder="Nombre"  autofocus value="{{$capa->nombre}}" > 
                        </div>
                        
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                          <input id="nombreCorto" type="text" readonly class="form-control col-md-12" name="nombreCorto" placeholder="Nombre corto"  autofocus value="{{$capa->nombreCorto}}" > 
                        </div>

                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                          <textarea id="descripcion" type="text" readonly class="form-control col-md-12" name="descripcion" placeholder="Descripción"  autofocus >{{$capa->descripcion}}</textarea>
                        </div>
                        <div class="input-group box-input">
                          <span class="input-group-addon"><i class="fas fa-calendar-alt"></i></span>
                          <input id="fechaExpiracion" type="date" readonly class="form-control col-md-12 " name="fechaExpiracion" placeholder="Fecha de expiración"  autofocus >
                        </div>
                      </div>		                    
                      
                      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 tabla">
                        <div><h4>Elementos existentes</h4></div>
                        <table id="detallesRegistrados" class="table table-striped table-bordered table-condensed table-hover ">
                          <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                            <th>Elemento</th>
                            <th>Nombre</th>
                            <th>Ancho</th>
                            <th>Etiqueta</th>
                            <th>Globo</th>
                            <th>Dátos</th>
                            <th>Origen</th>
                            <th>Observaciones</th>
                            <th>Opciones</th>
                          </thead>
                          <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tfoot>
                          <tbody>
                            @foreach($detalles as $detalle)
                              <tr class="selected" id="fila{{$detalle->id}}">
                                <td>
                                  <input type="hidden" name="elemento[]" value="{{$detalle->elemento}}">{{$detalle->elemento}}
                                </td>  
                                <td>
                                  <input type="hidden" value="{{$detalle->nombre}}">{{$detalle->nombre}}
                                </td> 
                                <td>
                                  <input type="hidden" value="{{$detalle->ancho}}">{{$detalle->ancho}}
                                </td>
                                <td>
                                  <input type="hidden" value="{{$detalle->etiqueta}}">{{$detalle->etiqueta}}
                                </td>
                                <td>
                                  <input type="hidden" value="{{$detalle->globo}}">{{$detalle->globo}}
                                </td>
                                <td>
                                  <input type="hidden" value="{{$detalle->datos}}">{{$detalle->datos}}
                                </td>
                                <td>
                                  <input type="hidden" value="{{$detalle->origenDatos}}">{{$detalle->origenDatos}}
                                </td>
                                <td>
                                  <input type="hidden" value="{{$detalle->observaciones}}">{{$detalle->observaciones}}
                                </td> 
                                <td>
                                  <button type="button" class="btn btn-warning" onclick="eliminar({{$detalle->id}});">X</button>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>



                <div class="row">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 tabla">

                        <div><h4>Nuevos elementos</h4></div>
                        <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="input-group box-input">
                              <span class="input-group-addon"><i class="fas fa-puzzle-piece"></i></span>
                              <select id="elemento" name="elemento" class="form-control" >
                                <option selected disabled value="0">Elemento</option>
                                <option value="Texto">Texto</option>
                                <option value="Combo">Combo</option>
                                <option value="Date">Date</option>
                              </select>
                            </div>
                              <div class="input-group box-input">
                                <span class="input-group-addon"><i class="fas fa-font"></i></span>
                                <input id="etiqueta" type="text" class="form-control col-md-12" name="etiqueta" placeholder="Etiqueta"  autofocus >
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6 col-md-offset-3">

                            <div class="input-group box-input">
                              <span class="input-group-addon"><i class="glyphicon fas fa-ruler"></i></span>
                              <input id="ancho" type="number" min="1" max="12" class="form-control col-md-12" name="ancho" placeholder="Ancho"  autofocus >
                            </div>



                            <div class="input-group box-input">
                              <span class="input-group-addon"><i class="glyphicon fas fa-info"></i></span>
                              <input id="globo" type="text" class="form-control col-md-12" name="globo" placeholder="Globo" autofocus >
                            </div>
                          </div>
                        </div>
                        
                          <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                            <div class="input-group box-input">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                              <textarea id="observaciones" type="text" class="form-control col-md-12" name="observaciones" placeholder="Observaciones"  autofocus ></textarea>
                            </div>
                          </div>
                        </div>
                          
                            <div class="row" id="wrapper-datos">
                                <div class="col-md-9 col-md-offset-4">
                                  <h2>Elige el tipo de origen de dato para el option</h2>
                                  <p>Puedes elegir dos origenes de datos: 1.- estáticos 2.- Desde una tabla en una BD</p>

                                    <div class="radio">
                                      <label><input type="radio" id="rbBb" name="rbOrigen" value="BD" onclick="limpiarTxtDatos()">Datos de BD</label>
                                    </div>
                                    <div class="radio">
                                      <label><input type="radio" id="rbEstaticos" name="rbOrigen" value="Crudos" onclick="limpiarTxtDatos()">Datos estáticos</label>
                                    </div>
                                </div>
                            </div>

                          <div class="row" id="wrapper-datos-tabla">
                                <br>
                                <br>
                                <div class="container">
                                  <div class="col-md-8 col-md-offset-2">                      
                                    <div class="col-md-4 col-md-offset-1">
                                      <h2>Nombre de la tabla</h2>
                                      <p>Selecciona el nombre de la tabla para visualizar sus columnas</p>
                                      <div id="dataDB">
                  
                                      </div>
                                    </div>
                                    <div class="col-md-4 col-md-offset-2">
                                      <h2>Nombre de las columnas</h2>
                                      <p>Selecciona el nombre de la columna que quieres que se visualice en el combo box</p>
                                      <div id="dataColumns">
                                          
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>


                            <div class="row" id="wrapper-datos-estaticos">
                              <br>
                              <br>                              
                              <div class="col-md-9 col-md-offset-3">
                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                    <input id="valor" type="text" class="form-control col-md-12" name="valor" placeholder="Valor"  autofocus >
                                  </div>
                                </div>

                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                    <input id="label" type="text" class="form-control col-md-12" name="label" placeholder="Label"  autofocus >
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-md-3">
                                    <button type="button" id="bt_add_option"  class="btn btn-primary" style="background-color:#12A3E7;color:white;border-color:#12A3E7;">
                                      <span class="submit-text">Agregar opción</span>
                                    </button>
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-8 col-md-offset-2">
                                  <table id="detalleOpciones" class="table table-striped table-bordered table-condensed table-hover ">
                                    <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                                      <th>Valor</th>
                                      <th>Label</th>
                                      <th>Opciones</th>
                                    </thead>
                                    <tfoot>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                   </table>
                              </div>
                            </div>


                            <div class="row" id="wrapper-txt-datos">
                              <br>
                              <br>
                              <br>
                              <div class="col-md-6 col-md-offset-3">
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                                  <textarea  id="datos" type="text" class="form-control col-md-12" name="datos" placeholder="Datos"  autofocus ></textarea>
                                </div>
                              </div>
                            </div>



                            <div class="row">
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-5">
                                  <button type="button" id="bt_add"  class="btn btn-primary" style="background-color:#12A3E7;color:white;border-color:#12A3E7;">
                                      <span class="submit-text">Agregar elemento </span>
                                      <span class="glyphicon glyphicon-send"></span>
                                  </button>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
              
                          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 tabla">
                          <table id="detalles" class="table table-striped table-bordered table-condensed table-hover ">
                            <thead style="background-color:#ff149b;color:white;text-align: center;font-size:12px">
                              <th>Elemento</th>
                              <th>Nombre</th>
                              <th>Ancho</th>
                              <th>Etiqueta</th>
                              <th>Globo</th>
                              <th>Dátos</th>
                              <th>Origen</th>
                              <th>Observaciones</th>
                              <th>Opciones</th>
                            </thead>
                            <tfoot>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                            </tfoot>
                            <tbody>
                            </tbody>
                           </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
                    <div class="form-group">
                      <input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
                      <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                  </div>

              {!!Form::close()!!} 
            </div>
         	</div>
       	</div>
    	</div>
	  </div>
  </div>
</div>
@push ('scripts')
<script src="{{ asset("js/global.js") }}"></script>
<script>
var capa = {!! json_encode($capa->toArray()) !!};
var detalles = {!! json_encode($detalles->toArray()) !!};
 $(document).ready(function(){
  $('#fechaExpiracion').val(capa.fechaExpiracion);
    $('#bt_add').click(function(){
      agregar()
    });

    $('#bt_add_option').click(function(){
      agregarOpcion()
    });

    $('#elemento').change(function(){
      if($("#elemento option:selected").text()=='Combo'){
        $('#wrapper-datos').show();
      }else{
        $('#wrapper-datos').hide();
        $('#wrapper-datos-estaticos').hide();
        $("#wrapper-datos-tabla").hide();
        $('#wrapper-txt-datos').hide();

        //LIMPIAR TABLA
        $("#detalleOpciones > tbody").children().remove();
        $('#datos').val("")
      }
    });

    $('#rbBb').click(function(){
      $("#wrapper-datos-tabla").show();
      $('#wrapper-txt-datos').show();
      $('#wrapper-datos-estaticos').hide();  
    });          
    $('#rbEstaticos').click(function(){
      $("#wrapper-datos-tabla").hide();
      $('#wrapper-datos-estaticos').show(); 
      $('#wrapper-txt-datos').show(); 
    });

    var tablasDB = {!! json_encode($tablas) !!};
    for (var i =0; i<tablasDB.length; i++) {
      if(tablasDB[i].table_name !== "migrations"){
        if(tablasDB[i].table_name !== "password_resets"){
          if(tablasDB[i].table_name != "tagging_tag_groups"){
            if(tablasDB[i].table_name != "tagging_tagged"){
              if(tablasDB[i].table_name != "tagging_tags"){
                if(tablasDB[i].table_name != "users"){
                  var radio =`<div class="radio"><label><input type="radio" name="optNombreTabla"value="${tablasDB[i].table_name}" onclick="selectRowsTable();" >${tablasDB[i].table_name}</label></div><div class="radio">`
                  $('#dataDB').append(radio);
                }
              }
            }
          }
        }
      }
    } 

  });


  var cont=0;
  var contOpt=0;
  var filas
  var tamanio=detalles.length;
  var contadorNombre=(detalles[tamanio-1].nombre.slice(1, detalles[tamanio-1].nombre.length));
  $("#guardar").hide();
  $('#wrapper-datos').hide();
  $("#wrapper-datos-tabla").hide();
  $('#wrapper-datos-estaticos').hide();
  $('#wrapper-txt-datos').hide();

    function selectRowsTable(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    var _token = $('input[name="_token"]').val();
    $('#datos').val('')
    $.ajax({
          type: "POST",
          url: urlImport+"obtener-columnas/"+nombre,
          
          data: { _token : _token },
        }).done(function(dataR) {
           $('#dataColumns').html('');
          for (var i =0; i<dataR.length; i++) {

            if(dataR[i].column_name != 'created_at'){
              if(dataR[i].column_name != 'updated_at'){
                var radio =`<div class="checkbox"><label><input type="checkbox" name="optNombreColumna"value="${dataR[i].column_name}" onclick="construirQuery()" > ${dataR[i].column_name}</label></div>`
                $('#dataColumns').append(radio);
              }
            }
          }  
        })
        .error(function(data) {
          alert("ERROR AL CONSULTAR")
          //swal('¡Error!', 'No se pudo eliminar la capa', "error");
        })
  }

 function agregar(){
    console.log(detalles.length);

    
    contadorNombre++;
    elemento=$("#elemento option:selected").text();
    ancho=$("#ancho").val();
    etiqueta=$("#etiqueta").val();
    globo=$("#globo").val();
    datos=$("#datos").val();
    observaciones=$("#observaciones").val();
    nombre="E"+contadorNombre;
    var filasTab
    var celdasTab
    var validacion=true
    filasTab=($('#detalles tr'))
    if(ancho<=12){
      for (var i =2; i< $("#detalles > tbody").children().length +2; i++){
          celdasTab = $(filasTab[i]).find('td')
          var dato =$(celdasTab[1])[0].innerText
          if(dato==nombre){
            alert("Ya hay un elemento con ese nombre")
            validacion=false
          }
      }
      filasTab=""
      celdasTab=""
      filasTab=($('#detallesRegistrados tr'))
      for (var i =2; i< $("#detallesRegistrados > tbody").children().length +2; i++){
        celdasTab = $(filasTab[i]).find('td')
        var dato =$(celdasTab[1])[0].innerText
        if(dato==nombre){
          alert("Ya hay un elemento registrado con ese nombre")
          validacion=false
        }
      }

      if(($("#elemento option:selected").text()=='Combo') && validacion==true){
        datos=$("#datos").val();
        let origenDatos = $('input[name="rbOrigen"]:checked').val();
        if (elemento!="" && nombre!="" && ancho!="" && etiqueta!="" && globo!=""&& datos!=""){
          var fila='+<tr class="selected" id="fila'+cont+'"><td><input type="hidden" name="elemento[]" value="'+elemento+'">'+elemento+'</td> <td><input type="hidden" name="nombreElemento[]" value="'+nombre+'">'+nombre+'</td> <td><input type="hidden" name="ancho[]" value="'+ancho+'">'+ancho+'</td><td><input type="hidden" name="etiqueta[]" value="'+etiqueta+'">'+etiqueta+'</td><td><input type="hidden" name="globo[]" value="'+globo+'">'+globo+'</td><td><input type="hidden" name="datos[]" value="'+datos+'">'+datos+'</td><td><input type="hidden" name="origenDatos[]" value="'+origenDatos+'">'+origenDatos+'</td><td><input type="hidden" name="observaciones[]" value="'+observaciones+'">'+observaciones+'</td> <td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td></tr>';
          cont++;
          limpiar();
          $('#detalles').append(fila);
          validar();
        }else{
        alert("Error al ingresar el elemento, revise los datos ingresados")
        }     
      }else if(validacion==true){
        if (elemento!="" && nombre!="" && ancho!="" && etiqueta!="" && globo!=""){
          var fila='+<tr class="selected" id="fila'+cont+'"><td><input type="hidden" name="elemento[]" value="'+elemento+'">'+elemento+'</td> <td><input type="hidden" name="nombreElemento[]" value="'+nombre+'">'+nombre+'</td> <td><input type="hidden" name="ancho[]" value="'+ancho+'">'+ancho+'</td><td><input type="hidden" name="etiqueta[]" value="'+etiqueta+'">'+etiqueta+'</td><td><input type="hidden" name="globo[]" value="'+globo+'">'+globo+'</td><td><input type="hidden" name="datos[]" value="">No aplica</td><td><input type="hidden" name="origenDatos[]" value="">No aplica</td><td><input type="hidden" name="observaciones[]" value="'+observaciones+'">'+observaciones+'</td> <td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td></tr>';
          cont++;
          limpiar();
          $('#detalles').append(fila);
          validar();
        }else{
        alert("Error al ingresar el elemento, revise los datos ingresados")
        }   
      }
    }
  }
function agregarOpcion(){

    valor=$("#valor").val();
    label=$("#label").val();

    var filasTab
    var celdasTab
    var validacion=true
    filasTab=($('#detalleOpciones tr'))


    for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
        celdasTab = $(filasTab[i]).find('td')
        var valorR = $(celdasTab[0])[0].innerText
        var labelR = $(celdasTab[1])[0].innerText
        if(valorR==valor){
          alert("Ya hay un elemento con ese valor")
          validacion=false
        }else if(labelR==label){
          alert("Ya hay un elemento con ese label")
          validacion=false
        }
    }

    if (validacion==true){
      if (valor!="" && label!=""){
        var fila='+<tr class="selected" id="filaOpt'+contOpt+'"><td><input type="hidden" name="valor[]" value="'+valor+'">'+valor+'</td><td><input type="hidden" name="label[]" value="'+label+'">'+label+'<td><button type="button" class="btn btn-warning" onclick="eliminarOpcion('+contOpt+');">X</button></td></tr>';
          contOpt++;
          limpiarOpt();
          $('#detalleOpciones').append(fila);
          validar();
      }else{
        alert("Error al ingresar el elemento, revise los datos ingresados")
      }
      construirJson();
    }
  }

  function limpiarTxtDatos(){
     $('#datos').val("")
  }

  function limpiarOpt(){
    $("#valor").val("");
    $("#label").val("");
  }
  function eliminarOpcion(index){   
    $("#filaOpt" + index).remove();
    construirJson()
  }

  function construirJson(){
    var JsonDatos=''
    $('#detalleOpciones tr').each(function(){
      celdas =  $(this).find('td'); 
  });

  filas=($('#detalleOpciones tr'))
  for (var i =2; i< $("#detalleOpciones > tbody").children().length +2; i++){
    celdas = $(filas[i]).find('td')
    if(i==$("#detalleOpciones > tbody").children().length +1){
      var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText
      JsonDatos= JsonDatos+dato
      $('#datos').val(JsonDatos)
    }else{
      var dato =$(celdas[0])[0].innerText+"-"+$(celdas[1])[0].innerText+", "
      JsonDatos= JsonDatos+dato
      $('#datos').val(JsonDatos)
    }
  }
 }

 function limpiar(){
    $("#nombreElemento").val("");
    $("#ancho").val("");
    $("#etiqueta").val("");
    $("#globo").val("");
    $("#datos").val("");
    $("#observaciones").val("");

  }

 function eliminar(index){ 
  var _token = $('input[name="_token"]').val();
    swal({
      title: '¿Estás seguro?',
      text: "¡No se podrán deshacer los cambios!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    })
    .then(function () {
      // código que elimina
      $.ajax({
        type: "delete",
        url: urlImport+"eliminar-detalle-modulo/"+index,
        data: { _token : _token },
      }).done(function(data) {
        swal('¡Eliminado!', 'El elemento ha sido eliminado.', "success");
        $("#fila" + index).remove();
        validar();
      })
      .error(function(data) {
        swal('¡Error!', 'No se pudo eliminar el elemento', "error");
      })
    },function (dismiss) {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelado',
          'No se han realizado cambios',
          'error'
        )
      }
    })
 }

 function quitarFila(index){   
    $("#fila" + index).remove();
    validar();
 }

 function construirQuery(){
    let nombre = $('input[name="optNombreTabla"]:checked').val();
    let columnas=''
    $('input[name="optNombreColumna"]:checked').each(function() {
      columnas= columnas+$(this).val()+",' - ',"
    });
    columnas= columnas.trim()
    columnas= columnas.substring(0,columnas.length-7);
    let query=  `SELECT id, CONCAT(${columnas})  AS Label FROM ${nombre}`
    $('#datos').val(query)
  }

 function validar(){
  var nFilas = $("#detalles > tbody").children().length;
  if (nFilas>0){
    $("#guardar").show()
  }else{
    $("#guardar").hide()
  }
 }

</script>
@endpush
@endsection