@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
   <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      .input-group-addon{
			padding:6px;
		}
    </style>
@endpush

@section('main_container')


<script>
  function validanumero(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	if (((keynum >= 48) && (keynum <= 57)) || keynum == 8)
        return true;

        return /\d/.test(String.fromCharCode(keynum));
}

function validaletra(e){
	tecla = (document.all) ? e.keyCode : e.which; // 2
if (tecla==8) return true; // 3
patron =/[A-Za-z-ñáéíóúÑÁÉÍÓÚ\s]/; // 4
te = String.fromCharCode(tecla); // 5
return patron.test(te); // 6
}
</script>
    <!-- page content -->
<div class="container">
  <div class="right_col" role="main">
    <div class="container" style="margin-top:7%">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Registro de escuelas</h2>
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="col-md-12"  method="POST" action="{{ route('escuelas.store') }}">

                {{ csrf_field() }}

                <div class="row ">
                  <div class="col-md-5">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                      <input id="nombre" type="text" class="form-control col-md-12" name="nombre" placeholder="Nombre"  autofocus required>
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                      <input id="cct" type="text" class="form-control col-md-12" name="cct" placeholder="CCT"  autofocus required>
                    </div>
                  </div>


							    <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="far fa-clock"></i></span>
                      <select id="turno" name="turno" class="form-control" required>
                        <option selected disabled value="0">Turno</option>
                        <option value="MATUTINO">Matutino</option>
                        <option value="VESPERTINO">Vespertino</option>
                        <option value="CONTINUO (TIEMPO COMPLETO)">Continuo (tiempo completo)</option>
                        <option value="CONTINUO (JORNADA AMPLIADA)">Continuo (jornada amplia)</option>
                        <option value="NOCTURNO">Nocturno</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-school"></i></span>
                      <select id="tipo" name="tipo" class="form-control" required>
                        <option selected disabled value="0">Tipo</option>
                        <option value="EDUCACIÓN BÁSICA">Educación básica</option>
                        <option value="EDUCACIÓN ESPECIAL">Educación especial</option>
                      </select>
                    </div>
                  </div>

                </div>

                <div class="row">
									

									<div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-school"></i></span>
                      <select id="nivel" name="nivel" class="form-control" required>
                        <option selected disabled value="0">Nivel</option>
                        <option value="PREESCOLAR">Preescolar</option>
                        <option value="PRIMARIA">Primaria</option>
                        <option value="SECUNDARIA">Secunadaria</option>
                        <option value="USAER">Usaer</option>
                        <option value="CAM">Cam</option>
                        <option value="INICIAL">Inicial</option>
                        <option value="DIRECCION">Direccion</option>
                        <option value="CEI">Cei</option>
                        <option value="OTRO">Otro</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-school"></i></span>
                      <select id="servicio" name="servicio" class="form-control" required>
                        <option selected disabled value="0">Servicio educativo</option>

                        <option value="PRIMARIA GENERAL">PRIMARIA GENERAL</option>
                        <option value="USAER">USAER</option>
                        <option value="PREESCOLAR GENERAL">PREESCOLAR GENERAL</option>
                        <option value="SECUNDARIA GENERAL">SECUNDARIA GENERAL</option>
                        
                        <option value="TELESECUNDARIA">TELESECUNDARIA</option>
                        <option value="SECUNDARIA TÉCNICA">SECUNDARIA TÉCNICA</option>
                        <option value="CAM">CAM</option>
                        <option value="SECUNDARIA P. TRAB.">SECUNDARIA P. TRAB.</option>
                        
                        <option value="CENDI">CENDI</option>                   
                        <option value="EDUCACIÓN PARA ADULTOS">EDUCACIÓN PARA ADULTOS</option>
                        <option value="PRIMARIA">PRIMARIA</option>
                        <option value="SECUNDARIA">SECUNDARIA</option>

                        <option value="PREESCOLAR">PREESCOLAR</option>
                        <option value="INICIAL">INICIAL</option>
                        <option value="CCACI">CCACI</option>
                        <option value="CEI">CEI</option>

                        <option value="CCAI">CCAI</option>
                        <option value="GENERAL">GENERAL</option>
                        <option value="ESPECIAL">ESPECIAL</option>
                        <option value="REGIONAL">REGIONAL</option>

                        <option value="ESTANCIA INFANTIL">ESTANCIA INFANTIL</option>      
                        <option value="MATEMATECA">MATEMATECA</option>
                        <option value="LUDOTECA">LUDOTECA</option>
                        <option value="ALMACEN">ALMACEN</option>

                        <option value="CADI">CADI</option>
                        <option value="OTRO">OTRO</option>

                      </select>
                    </div>
                  </div>


									<div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                      <input id="calle" type="text" class="form-control" name="calle" placeholder="Calle" required>
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                      <input id="numero" type="text" class="form-control" name="numero" placeholder="Número" required>
                    </div>
                  </div>


									<div class="col-md-2">
                    <div class="input-group">
                     	<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                     	<select id="delegacion" name="delegacion" class="form-control" required>
                     		<option selected disabled >Delegación</option>
                       		@foreach($delegaciones as $delegacion)
                         		<option value="{{ $delegacion->id }}">{{ $delegacion->nombre }}</option>
                       		@endforeach
                     	</select>
                    </div>
                  </div>



								  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                      <input id="colonia" type="text" class="form-control" name="colonia" placeholder="Colonia"  required>
                    </div>
                  </div>


							    <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                      <input id="cp" type="text" class="form-control" name="cp" placeholder="Código Postal" onKeypress="return validanumero(event)" required>
                    </div>
                  </div>


							    <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-school"></i></span>
                      <input id="idInmueble" type="text" class="form-control" name="idInmueble" placeholder="Id inmueble" required>
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-user-tie"></i></span>
                      <select id="control" name="control" class="form-control" required>
                        <option selected disabled value="0">Contról</option>
                        <option value="PÚBLICO">PÚBLICO</option>
                        <option value="PRIVADO">PRIVADO</option>
                        <option value="DIF">DIF</option>
                        <option value="DIF CDMX">DIF CDMX</option>
                        <option value="GCDMX">GCDMX</option>
                        <option value="COMUNITARIO">COMUNITARIO</option>
                        <option value="FEDERAL">FEDERAL</option>
                        <option value="PARTICULAR">PARTICULAR</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-child"></i>
                      </span>
                      <input id="totalAlumnos" type="number" class="form-control" name="totalAlumnos" placeholder="Total de alumnos" required>
                    </div>
                  </div>
                </div>

                <p style="padding: 5px;">
                <div class="col-md-4">
                  <p>
                </div>
                <div class="row"></div>
                <br>

                  <div class="row">
                    <div class="x_panel">
                      <div class="x_title" style="text-align:center;">
                        <h2>Seleccionar ubicación</h2>
      							    <div class="col-md-offset-2 col-md-2">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            <input id="lat" type="text" class="form-control" name="lat" placeholder="Latitud"  required readonly>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            <input id="lng" type="text" class="form-control" name="lng" placeholder="Longuitud" required readonly>
                          </div>
                        </div>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                        <div class="x_content">
                          <input id="pac-input" class="controls" type="text" placeholder="Buscar en Google Maps">
                          <div id="map"></div>
                        </div>
                      </div>
                  </div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-5">
                    <button type="submit"  class="btn btn-primary" style="background-color:#ff149b;color:white;border-color:#ff149b;">
                      <span class="submit-text">Registrar</span>
                      <span class="glyphicon glyphicon-send"></span>
                    </button>
                  </div>
                </div>  
              </form>	
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">

        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdDlVBYRQHwNjtagRUt6L4TZcZ9oo-XJk&libraries=places&callback=initAutocomplete"
         async defer></script>
@endsection
<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
      var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 19.311228, lng: -99.136524},
      zoom: 10,
      mapTypeId: 'roadmap'
      });

      var cdmxCoords = [
        {lng: -99.21310300240002,lat: 19.5043531549}, {lng: -99.21106002170001,lat: 19.5121997572}, {lng: -99.206021037,lat: 19.5159939395}, {lng: -99.1792625457,lat: 19.5071568627}, {lng: -99.17488913749999,lat: 19.5092377666}, {lng: -99.158260035,lat: 19.5039384159}, {lng: -99.16401954680001,lat: 19.5189413674}, {lng: -99.1762142155,lat: 19.5247177796}, {lng: -99.17222159729998,lat: 19.5282379634}, {lng: -99.1814707967,lat: 19.5336329672}, {lng: -99.1601638283,lat: 19.5278392366}, {lng: -99.15674843780001,lat: 19.533466061}, {lng: -99.15910904250001,lat: 19.535821652}, {lng: -99.15211478480001,lat: 19.5457034338}, {lng: -99.16148910550001,lat: 19.5501570391}, {lng: -99.15941121180001,lat: 19.5590497656}, {lng: -99.1504816831,lat: 19.563064418}, {lng: -99.1473549608,lat: 19.5680870141}, {lng: -99.13800863909999,lat: 19.5828424753}, {lng: -99.1236056613,lat: 19.5932278843}, {lng: -99.118444825,lat: 19.5907371194}, {lng: -99.1197389993,lat: 19.5860565753}, {lng: -99.1102998535,lat: 19.5651362924}, {lng: -99.11029058050001,lat: 19.5651225186}, {lng: -99.1310643579,lat: 19.5376713066}, {lng: -99.12936376739999,lat: 19.5271148388}, {lng: -99.1205188556,lat: 19.5148848664}, {lng: -99.11483198610002,lat: 19.5120941063}, {lng: -99.10733430059999,lat: 19.5108619099}, {lng: -99.099842947,lat: 19.5143023831}, {lng: -99.09188593899998,lat: 19.5108397957}, {lng: -99.0650663091,lat: 19.4994274036}, {lng: -99.0686609807,lat: 19.4900285202}, {lng: -99.05162902019998,lat: 19.4500041903}, {lng: -99.04830367629999,lat: 19.4424617618}, {lng: -99.0557176234,lat: 19.4256077243}, {lng: -99.05903322250001,lat: 19.4015549011}, {lng: -99.0197404876,lat: 19.3837192738}, {lng: -98.9913022281,lat: 19.3669496024}, {lng: -98.99401027579999,lat: 19.3575589013}, {lng: -98.9695724206,lat: 19.3329793475}, {lng: -98.96429821940001,lat: 19.3284105918}, {lng: -98.9581602903,lat: 19.3236295503}, {lng: -98.96324688439999,lat: 19.3168227372}, {lng: -98.9651118938,lat: 19.3067478203}, {lng: -98.96785405919999,lat: 19.3057715643}, {lng: -98.9761018497,lat: 19.2529526812}, {lng: -98.9669417998,lat: 19.2507765873}, {lng: -98.9695502097,lat: 19.2329122797}, {lng: -98.94566588329998,lat: 19.2249400758}, {lng: -98.9414716413,lat: 19.223500993}, {lng: -98.94390996289998,lat: 19.2163976831}, {lng: -98.95195866730001,lat: 19.2186684075}, {lng: -98.9685472577,lat: 19.2118091662}, {lng: -98.96777134440001,lat: 19.1985745181}, {lng: -98.96616020179999,lat: 19.1860256888}, {lng: -98.9569156805,lat: 19.177114495}, {lng: -98.95349278019998,lat: 19.1693435252}, {lng: -98.96801879089999,lat: 19.1653613283}, {lng: -98.9627711079,lat: 19.1594571961}, {lng: -98.95482840230001,lat: 19.1504090001}, {lng: -98.9691953224,lat: 19.1457666808}, {lng: -98.95884629649999,lat: 19.1218421485}, {lng: -98.9623134531,lat: 19.0969846402}, {lng: -98.9688346432,lat: 19.0843844092}, {lng: -98.97333935790002,lat: 19.0815022765}, {lng: -98.979988345,lat: 19.075577502}, {lng: -98.9860309207,lat: 19.0803229277}, {lng: -99.0295639476,lat: 19.0865145377}, {lng: -99.04453829929999,lat: 19.0766835183}, {lng: -99.0610576226,lat: 19.0497728775}, {lng: -99.13440162950001,lat: 19.0877483025}, {lng: -99.22690899610001,lat: 19.0969212633}, {lng: -99.28050066600001,lat: 19.1320869049}, {lng: -99.3038067886,lat: 19.1909599309}, {lng: -99.30819375420001,lat: 19.2143076935}, {lng: -99.31638151729999,lat: 19.2219418775}, {lng: -99.31691535669999,lat: 19.2263507764}, {lng: -99.317612537,lat: 19.2302143355}, {lng: -99.3242005127,lat: 19.233366085}, {lng: -99.3406104828,lat: 19.2412069406}, {lng: -99.3430869475,lat: 19.2419140008}, {lng: -99.3431676142,lat: 19.2515414665}, {lng: -99.33898715820001,lat: 19.2648847921}, {lng: -99.3405100781,lat: 19.2685294875}, {lng: -99.349803569,lat: 19.275008012}, {lng: -99.3654731569,lat: 19.2789979349}, {lng: -99.35563230199999,lat: 19.2944212315}, {lng: -99.3513988942,lat: 19.2951836101}, {lng: -99.35754956060002,lat: 19.3078556805}, {lng: -99.33345889650001,lat: 19.3327792758}, {lng: -99.327898527,lat: 19.3462035917}, {lng: -99.32970312850001,lat: 19.3535558944}, {lng: -99.32441236450001,lat: 19.3589778619}, {lng: -99.3197408052,lat: 19.3590837386}, {lng: -99.32063376030001,lat: 19.3656724912}, {lng: -99.304355007,lat: 19.3786196972}, {lng: -99.30184374949999,lat: 19.3776235647}, {lng: -99.29995472669998,lat: 19.3683138335}, {lng: -99.28644713160001,lat: 19.376729077}, {lng: -99.2838612739,lat: 19.3819147746}, {lng: -99.27776428639999,lat: 19.3753663797}, {lng: -99.27377670510002,lat: 19.390507911}, {lng: -99.27045469419998,lat: 19.3908947672}, {lng: -99.26643088570002,lat: 19.3991700681}, {lng: -99.26158196260001,lat: 19.4004311428}, {lng: -99.25858991410001,lat: 19.4053192267}, {lng: -99.25758610690001,lat: 19.401063919}, {lng: -99.2513899068,lat: 19.4085293807}, {lng: -99.23943744729999,lat: 19.4129039212}, {lng: -99.2238741181,lat: 19.4280725034}, {lng: -99.2289555107,lat: 19.4390032365}, {lng: -99.2192213795,lat: 19.4450165411}, {lng: -99.2206945012,lat: 19.4538854469}, {lng: -99.2156666952,lat: 19.4577183997}, {lng: -99.21295757740001,lat: 19.468285328}, {lng: -99.2079767948,lat: 19.472118959}, {lng: -99.22204802330001,lat: 19.4757300108}, {lng: -99.21310300240002,lat: 19.5043531549}
      ];

      // Construct the polygon.
      var cdmx = new google.maps.Polygon({
        paths: cdmxCoords,
        strokeColor: '#ff149b',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#ff149b',
        fillOpacity: 0.1
      });
      cdmx.setMap(map);
      geocoder = new google.maps.Geocoder();

      // Create the search box and link it to the UI element.
      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });

      var markers = [];
      var marker;

      google.maps.event.addListener(cdmx, 'click', function(event) {

        geocoder.geocode(
          {'latLng': event.latLng},
          function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                var posicion = results[0].geometry.location;
                posicion = posicion.toString();
                posicion = posicion.replace("(","");
                posicion = posicion.replace(")","");
                posicion = posicion.split(",",2);
                document.getElementById('lat').value = posicion[0];
                document.getElementById('lng').value = posicion[1];
                if (marker) {
                  marker.setPosition(event.latLng);
                } else {
                  marker = new google.maps.Marker({
                  position: event.latLng,
                  map: map});
                }
              } else {
                document.getElementById('geocoding').innerHTML =
                'No se encontraron resultados';
              }
          } else {
            document.getElementById('geocoding').innerHTML =
            'Geocodificación  ha fallado debido a: ' + status;
          }
        }); 
        //for(x=0;x<markers.length;x++){markers[x].setVisible(false);}marker.setVisible(true);
      });

      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          // Create a marker for each place.
          var posicion = place.geometry.location;
          posicion = posicion.toString();
          posicion = posicion.replace("(","");
          posicion = posicion.replace(")","");
          posicion = posicion.split(",",2);

          document.getElementById('lat').value = posicion[0];
          document.getElementById('lng').value = posicion[1];
          markers.push(new google.maps.Marker({
            map: map,
            title: place.name,
            position: place.geometry.location
          }));
          marker.setVisible(false);
          if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
    }
    </script>
