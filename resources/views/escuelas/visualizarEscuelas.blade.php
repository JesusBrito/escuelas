
@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->     
@endpush

@section('main_container')

<!-- page content -->
<div class="container">
  <div class="right_col" role="main">
    <div class="container" style="margin-top:7%">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Lista de escuelas</h2>
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <div class="row">

                  <div class="col-md-4">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                      <select id="delegacion" name="delegacion" class="form-control" onchange="delegacion(this)">
                        <option selected disabled >Delegación</option>
                          @foreach($delegaciones as $delegacion)
                            <option value="{{ $delegacion->nombre }}">{{ $delegacion->nombre }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <table id="datatable"  class="table table-striped table-bordered ">
                  <thead>
                    <tr style="background-color:#ff149b;color:white;text-align: center;font-size:12px">      
                      <th>Nombre</th>     
                      <th>CCT</th>
                      <th>Nivel</th>
                      <th>Servicio</th>
                      <th>Delegación</th>  
                      <th>Opciones</th>    
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($escuelas as $escuela)
                      <tr style="font-size:12px">    
                        <td>{{ $escuela->nombre }}</td>     
                        <td>{{ $escuela->cct }}</td>
                        <td>{{ $escuela->nivel_educativo }}</td>
                        <td>{{ $escuela->servicio_educativo }}</td>
                        <td>
                          @foreach ($delegaciones as $delegacion)
                            @if($delegacion->id==$escuela->delegacion_id)
                               {{$delegacion->nombre}}
                            @endif
                          @endforeach
                        </td>
                        <td style="text-align:center" width="8%">
                          <div style="display: inline-block;">
                            <a  href="{{ url("escuelas/{$escuela->id}/edit") }}"><img src="{{ URL::asset("images") }}/edit.png"" alt="Editar información" title="Editar información"></a>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                  </tbody> 
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
<!-- /footer content -->
<footer>
  <div class="pull-right">
  </div>
  <div class="clearfix"></div>
  
</footer>
@push ('scripts')
  <script>
    function delegacion(id){
        var delegacion = document.getElementById("delegacion").value;    
        $("#datatable_filter :input")[0].value=delegacion
        $('#datatable_filter :input').trigger('keyup');
    }
    $(document).ready(()=>{
      $('#datatable').on( 'draw', function () {
        alert( 'Procesando datos' )
      })

      $('#datatable').DataTable({
        language: {
          search:        "Buscar:",
          lengthMenu:    "Mostrar _MENU_ registros",
          info:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          infoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
          infoFiltered:   "(filtrado de un total de _MAX_ registros)",
          infoPostFix:    "",
          loadingRecords: "Cargando...",
          zeroRecords:    "No se encontraron resultados",
          emptyTable:     "Ningún dato disponible en esta tabla",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Último"
          },
          aria: {
              sortAscending:  ": Activar para ordenar la columna de manera ascendente",
              sortDescending: ": Activar para ordenar la columna de manera descendente"
          },
          destroy: true,
        }
      })
    })

  </script>
  @endpush
@endsection