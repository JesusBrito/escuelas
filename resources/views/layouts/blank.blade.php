<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mapa escolar</title>

        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("css/dataTables.min.css") }}" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/custom.min.css") }}" rel="stylesheet">
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
        <link href="{{ asset("css/fixedHeader.bootstrap.min.css") }}" rel="stylesheet">



        <!--<link href="{{ asset("css/sweetalert.css") }}" rel="stylesheet">-->
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@6.6.2/dist/sweetalert2.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@6.6.2/dist/sweetalert2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> 
        @stack('stylesheets')

        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

                @include('includes/footer')

            </div>
        </div>
        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        @stack('scripts')

        <script src="{{ asset("js/bootstrap.min.js") }}"></script>
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <script src="{{ asset("js/dataTables.min.js") }}"></script>
        <script src="{{ asset("js/moment.js") }}"></script>
        <!--<script src="{{ asset("js/sweetalert.min.js") }}"></script>-->
    </body>
</html>
