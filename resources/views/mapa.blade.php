
<script>
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      var x
      var markers = [];
      var marker;
      var sites = {!! json_encode($escuelas->toArray()) !!};
      var delegaciones = {!! json_encode($delegaciones->toArray()) !!};
      var timeout=null;

      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: 19.34228, lng: -99.136524},
        });

        var cdmxCoords = [
           {lng: -99.21310300240002,lat: 19.5043531549}, {lng: -99.21106002170001,lat: 19.5121997572}, {lng: -99.206021037,lat: 19.5159939395}, {lng: -99.1792625457,lat: 19.5071568627}, {lng: -99.17488913749999,lat: 19.5092377666}, {lng: -99.158260035,lat: 19.5039384159}, {lng: -99.16401954680001,lat: 19.5189413674}, {lng: -99.1762142155,lat: 19.5247177796}, {lng: -99.17222159729998,lat: 19.5282379634}, {lng: -99.1814707967,lat: 19.5336329672}, {lng: -99.1601638283,lat: 19.5278392366}, {lng: -99.15674843780001,lat: 19.533466061}, {lng: -99.15910904250001,lat: 19.535821652}, {lng: -99.15211478480001,lat: 19.5457034338}, {lng: -99.16148910550001,lat: 19.5501570391}, {lng: -99.15941121180001,lat: 19.5590497656}, {lng: -99.1504816831,lat: 19.563064418}, {lng: -99.1473549608,lat: 19.5680870141}, {lng: -99.13800863909999,lat: 19.5828424753}, {lng: -99.1236056613,lat: 19.5932278843}, {lng: -99.118444825,lat: 19.5907371194}, {lng: -99.1197389993,lat: 19.5860565753}, {lng: -99.1102998535,lat: 19.5651362924}, {lng: -99.11029058050001,lat: 19.5651225186}, {lng: -99.1310643579,lat: 19.5376713066}, {lng: -99.12936376739999,lat: 19.5271148388}, {lng: -99.1205188556,lat: 19.5148848664}, {lng: -99.11483198610002,lat: 19.5120941063}, {lng: -99.10733430059999,lat: 19.5108619099}, {lng: -99.099842947,lat: 19.5143023831}, {lng: -99.09188593899998,lat: 19.5108397957}, {lng: -99.0650663091,lat: 19.4994274036}, {lng: -99.0686609807,lat: 19.4900285202}, {lng: -99.05162902019998,lat: 19.4500041903}, {lng: -99.04830367629999,lat: 19.4424617618}, {lng: -99.0557176234,lat: 19.4256077243}, {lng: -99.05903322250001,lat: 19.4015549011}, {lng: -99.0197404876,lat: 19.3837192738}, {lng: -98.9913022281,lat: 19.3669496024}, {lng: -98.99401027579999,lat: 19.3575589013}, {lng: -98.9695724206,lat: 19.3329793475}, {lng: -98.96429821940001,lat: 19.3284105918}, {lng: -98.9581602903,lat: 19.3236295503}, {lng: -98.96324688439999,lat: 19.3168227372}, {lng: -98.9651118938,lat: 19.3067478203}, {lng: -98.96785405919999,lat: 19.3057715643}, {lng: -98.9761018497,lat: 19.2529526812}, {lng: -98.9669417998,lat: 19.2507765873}, {lng: -98.9695502097,lat: 19.2329122797}, {lng: -98.94566588329998,lat: 19.2249400758}, {lng: -98.9414716413,lat: 19.223500993}, {lng: -98.94390996289998,lat: 19.2163976831}, {lng: -98.95195866730001,lat: 19.2186684075}, {lng: -98.9685472577,lat: 19.2118091662}, {lng: -98.96777134440001,lat: 19.1985745181}, {lng: -98.96616020179999,lat: 19.1860256888}, {lng: -98.9569156805,lat: 19.177114495}, {lng: -98.95349278019998,lat: 19.1693435252}, {lng: -98.96801879089999,lat: 19.1653613283}, {lng: -98.9627711079,lat: 19.1594571961}, {lng: -98.95482840230001,lat: 19.1504090001}, {lng: -98.9691953224,lat: 19.1457666808}, {lng: -98.95884629649999,lat: 19.1218421485}, {lng: -98.9623134531,lat: 19.0969846402}, {lng: -98.9688346432,lat: 19.0843844092}, {lng: -98.97333935790002,lat: 19.0815022765}, {lng: -98.979988345,lat: 19.075577502}, {lng: -98.9860309207,lat: 19.0803229277}, {lng: -99.0295639476,lat: 19.0865145377}, {lng: -99.04453829929999,lat: 19.0766835183}, {lng: -99.0610576226,lat: 19.0497728775}, {lng: -99.13440162950001,lat: 19.0877483025}, {lng: -99.22690899610001,lat: 19.0969212633}, {lng: -99.28050066600001,lat: 19.1320869049}, {lng: -99.3038067886,lat: 19.1909599309}, {lng: -99.30819375420001,lat: 19.2143076935}, {lng: -99.31638151729999,lat: 19.2219418775}, {lng: -99.31691535669999,lat: 19.2263507764}, {lng: -99.317612537,lat: 19.2302143355}, {lng: -99.3242005127,lat: 19.233366085}, {lng: -99.3406104828,lat: 19.2412069406}, {lng: -99.3430869475,lat: 19.2419140008}, {lng: -99.3431676142,lat: 19.2515414665}, {lng: -99.33898715820001,lat: 19.2648847921}, {lng: -99.3405100781,lat: 19.2685294875}, {lng: -99.349803569,lat: 19.275008012}, {lng: -99.3654731569,lat: 19.2789979349}, {lng: -99.35563230199999,lat: 19.2944212315}, {lng: -99.3513988942,lat: 19.2951836101}, {lng: -99.35754956060002,lat: 19.3078556805}, {lng: -99.33345889650001,lat: 19.3327792758}, {lng: -99.327898527,lat: 19.3462035917}, {lng: -99.32970312850001,lat: 19.3535558944}, {lng: -99.32441236450001,lat: 19.3589778619}, {lng: -99.3197408052,lat: 19.3590837386}, {lng: -99.32063376030001,lat: 19.3656724912}, {lng: -99.304355007,lat: 19.3786196972}, {lng: -99.30184374949999,lat: 19.3776235647}, {lng: -99.29995472669998,lat: 19.3683138335}, {lng: -99.28644713160001,lat: 19.376729077}, {lng: -99.2838612739,lat: 19.3819147746}, {lng: -99.27776428639999,lat: 19.3753663797}, {lng: -99.27377670510002,lat: 19.390507911}, {lng: -99.27045469419998,lat: 19.3908947672}, {lng: -99.26643088570002,lat: 19.3991700681}, {lng: -99.26158196260001,lat: 19.4004311428}, {lng: -99.25858991410001,lat: 19.4053192267}, {lng: -99.25758610690001,lat: 19.401063919}, {lng: -99.2513899068,lat: 19.4085293807}, {lng: -99.23943744729999,lat: 19.4129039212}, {lng: -99.2238741181,lat: 19.4280725034}, {lng: -99.2289555107,lat: 19.4390032365}, {lng: -99.2192213795,lat: 19.4450165411}, {lng: -99.2206945012,lat: 19.4538854469}, {lng: -99.2156666952,lat: 19.4577183997}, {lng: -99.21295757740001,lat: 19.468285328}, {lng: -99.2079767948,lat: 19.472118959}, {lng: -99.22204802330001,lat: 19.4757300108}, {lng: -99.21310300240002,lat: 19.5043531549}
        ];

        // Construct the polygon.
        var cdmx = new google.maps.Polygon({
          paths: cdmxCoords,
          strokeColor: '#EC008B',
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: '#EC008B',
          fillOpacity: 0.1
        });

        cdmx.setMap(map);
        var control = document.getElementById('controls');
        var div = document.createElement('div');
        div.innerHTML = '';
        control.appendChild(div);
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(control);
        /*
        var legend = document.getElementById('legend');
        var div = document.createElement('div');
        div.innerHTML = '';
        legend.appendChild(div);
        map.controls[google.maps.ControlPosition.LEFT_CENTER].push(legend);

        */

        

        for(x=0;x<sites.length;x++){  

          if (sites[x].lat==null){
            
          }
          else{  
            var lat = sites[x].lat;
            var lng = sites[x].lng;
            var delegacionid = sites[x].delegacion_id;
            var tipo = sites[x].tipo_educativo;
            var nivel = sites[x].nivel_educativo;
            var servicio = sites[x].servicio_educativo;
            var cct = sites[x].cct;
            var alumnos = sites[x].total_alumnos;
            var direccion = '';
            var control= sites[x].control;
            var idEscuela= sites[x].id;

                       
          
            direccion = sites[x].direccion + " " +sites[x].numero+ " " +sites[x].colonia+ " " +sites[x].cp;


            marcador= new google.maps.LatLng(lat,lng);
            
            var contentString = 

            '<table class="col-md-12 table table-hover informacion-escuela">'+
              '<thead style="background-color:#EC008B;color:white">'+
                '<tr>'+                                       
                  '<th colspan="4">'+sites[x].nombre+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.google.com.mx/maps?saddr=My+Location&daddr='+sites[x].lat+','+sites[x].lng+'" target="_blank"><img src="images/direcciones.png"></a>'+
                  '</th>'+ 
                '</tr>'+
              '</thead>'+
              '<tbody style="font-size:14px">'+
                '<tr>'+
                  '<td><b>CCT:</b></td>'+
                  '<td>'+sites[x].cct+'</td>'+
                  '<td><b>Turno:</b></td>'+
                  '<td>'+sites[x].turno+'</td>'+
                '</tr>'+
                '<tr>'+
                  '<td><b>Tipo:</b></td>'+
                  '<td>'+sites[x].tipo_educativo+'</td>'+
                  '<td><b>Nivel:</b></td>'+
                  '<td>'+sites[x].nivel_educativo+'</td>'+
                '</tr>'+
                '<tr>'+
                  '<td><b>Servicios:</b></td>'+
                  '<td>'+servicio+'</td>'+
                  '<td><b>Calle:</b></td>'+
                  '<td>'+sites[x].direccion+'</td>'+
                '</tr>'+ 
                '<tr>'+
                  '<td><b>Colonia:</b></td>'+
                  '<td>'+sites[x].colonia+'</td>'+
                  '<td><b>Número:</b></td>'+
                  '<td>'+sites[x].numero+'</td>'+
                '</tr>'+
                '<tr>'+
                  '<td><b>Delegación:</b></td>'+
                  '<td>'+delegaciones[delegacionid-1].nombre+'</td>'+
                '</tr>'+
              '</tbody>'+
            '</table>'+
            '<div class="col-md-2">'+
              '<h4>Capas:</h4>'+
              '<div id="listaCapas'+sites[x].id+'">'+
              '</div>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });
            
            marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolPrivada.png',
                  type: delegacionid,
                  school: control,
                  id:idEscuela,
                  map: map,        
            });
            
            /*
            if(sites[x].control=='PÚBLICO'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolPublica.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });  
            }else if(sites[x].control=='PRIVADO'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolPrivada.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }else if(sites[x].control=='DIF'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolDif.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }else if(sites[x].control=='DIF CDMX'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/DifCDMX.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }else if(sites[x].control=='GCDMX'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolCDMX.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });  
            }else if(sites[x].control=='COMUNITARIO'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolComunitario.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }else if(sites[x].control=='FEDERAL'){
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolFederal.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }else{
              marker = new google.maps.Marker({
                  position: marcador,
                  icon: '{{ URL::asset("images") }}/schoolParticular.png',
                  type: delegacionid,
                  school: control,
                  map: map,        
              });
            }
            */
            
          marker.setVisible(false);
          markers.push(marker);
           
          
          var currentinfowindow = null;
          google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){

            return function() {
              if (currentinfowindow != null) { 
                currentinfowindow.close(); 
              }
              infowindow.setContent(content);
              infowindow.open(map,marker);
              currentinfowindow = infowindow; 
              buscarCapas(marker)
            };
          })(marker,contentString,infowindow));         
            
          }
        }    
      }

      function controlFiltrado(valor){
        for (var i = 0; i < markers.length; i++) {
          markers[i].setVisible(false);
        }
        var contador=0;
        $("input:checkbox:checked").each(function() {
            contador++
        });

        if(contador>0){
          filtrarxCapa(valor)
        }else{
          tipoEscuela()
        }
      }

      function tipoEscuela(){
        var tipo = document.getElementById("tipoEscuela").value;
        var delegacion = document.getElementById("delegacion").value;
        for (var i = 0; i < markers.length; i++) {
          markers[i].setVisible(false);
        }

        if(tipo==0 && delegacion==0){
          for (var i = 0; i < markers.length; i++) {
             markers[i].setVisible(true);           
          }

        }else if(tipo=='NoOpt' && delegacion>0){
          for (var i = 0; i < markers.length; i++) {
            if(markers[i].type==delegacion){
              markers[i].setVisible(true);
            }else{
              markers[i].setVisible(false);
            }
          } 

        }else if(tipo!=0 && tipo!='NoOpt' && delegacion=='NoOpt'){
          for (var i = 0; i < markers.length; i++) {
            if(markers[i].school==tipo){
              markers[i].setVisible(true);
            }else{
              markers[i].setVisible(false);
            }
          } 
        }else if(tipo==0 && delegacion>0){
          for (var i = 0; i < markers.length; i++) {
            if(markers[i].type==delegacion){
              markers[i].setVisible(true);
            }else{
              markers[i].setVisible(false);
            }
          }
        }else if(tipo!=0 &&tipo!='NoOpt' && delegacion==0){
          for (var i = 0; i < markers.length; i++) {
            if(markers[i].school==tipo){
             markers[i].setVisible(true);
            }else{
              markers[i].setVisible(false);
            }
          }
        }else if(tipo!=0 &&tipo!='NoOpt' && delegacion>0){
          for (var i = 0; i < markers.length; i++){
            if(markers[i].school==tipo && markers[i].type==delegacion){
             markers[i].setVisible(true);
            }else{
              markers[i].setVisible(false);
            }
          }           
        }
      }


      function filtrarxCapa(capa){



        var tipo = document.getElementById("tipoEscuela").value;
        var delegacion = document.getElementById("delegacion").value;
        var _token = $('input[name="_token"]').val();
        var ids='{"idCapas":[]}';

        clearTimeout(timeout);
        timeout = setTimeout(function (){

          var obj = JSON.parse(ids);
          $("input:checkbox:checked").each(function() {
            obj['idCapas'].push({"Id":$(this).val()});
          });
          
          if(obj['idCapas'].length>0 && tipo!=0 &&tipo!='NoOpt' && delegacion>0){
            $.ajax({
              type: "POST",
              url: urlImport+"obtener-escuelas-capas",
              data: { _token : _token, idCapasReq:obj, idTipoReq:tipo, idDelegReq:delegacion },
              beforeSend: function() {
                $('#myModal').modal('show'); 
              }
            }).done(function(dataR) {
                for (var x = 0; x < dataR.length; x++) {
                  if(dataR[x].length==0){
                    swal('¡Alerta!', 'No hay escuelas asignadas a esa capa', "info");
                    $("#M"+capa.value).removeAttr("checked");
                  }else if(markers.length==dataR[x].length){
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setVisible(true);
                    }
                  }else{
                    for (var i = 0; i < markers.length; i++) {
                        for(var j = 0; j < dataR[x].length; j++){
                          if(markers[i].id==dataR[x][j].id && markers[i].school==tipo && markers[i].type==delegacion){
                            markers[i].setVisible(true);
                          }
                        } 
                      }
                    }         
              }
              $('#myModal').modal('hide');


            }).error(function(data) {
                alert("ERROR AL CONSULTAR")
                $('#myModal').modal('hide');
            })
          }else if(obj['idCapas'].length>0 && tipo!=0 &&tipo!='NoOpt' ){
            $.ajax({
              type: "POST",
              url: urlImport+"obtener-escuelas-capas",
              data: { _token : _token, idCapasReq:obj, idTipoReq:tipo, idDelegReq:delegacion },
              beforeSend: function() {
                $('#myModal').modal('show'); 
              }
            }).done(function(dataR) {
                for (var x = 0; x < dataR.length; x++) {
                  if(dataR[x].length==0){
                    swal('¡Alerta!', 'No hay escuelas asignadas a esa capa', "info");
                    $("#M"+capa.value).removeAttr("checked");
                  }else if(markers.length==dataR[x].length){
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setVisible(true);
                    }
                  }else{
                    for (var i = 0; i < markers.length; i++) {
                        for(var j = 0; j < dataR[x].length; j++){
                          if(markers[i].id==dataR[x][j].id && markers[i].school==tipo){
                            markers[i].setVisible(true);
                          }
                        } 
                      }
                    }         
              }
              $('#myModal').modal('hide');


            }).error(function(data) {
                alert("ERROR AL CONSULTAR")
                $('#myModal').modal('hide');
            })
          }else if(obj['idCapas'].length>0 && delegacion>0){
            $.ajax({
              type: "POST",
              url: urlImport+"obtener-escuelas-capas",
              data: { _token : _token, idCapasReq:obj, idTipoReq:tipo, idDelegReq:delegacion },
              beforeSend: function() {
                $('#myModal').modal('show'); 
              }
            }).done(function(dataR) {
                for (var x = 0; x < dataR.length; x++) {
                  if(dataR[x].length==0){
                    swal('¡Alerta!', 'No hay escuelas asignadas a esa capa', "info");
                    $("#M"+capa.value).removeAttr("checked");
                  }else if(markers.length==dataR[x].length){
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setVisible(true);
                    }
                  }else{
                    for (var i = 0; i < markers.length; i++) {
                        for(var j = 0; j < dataR[x].length; j++){
                          if(markers[i].id==dataR[x][j].id && markers[i].type==delegacion){
                            markers[i].setVisible(true);
                          }
                        } 
                      }
                    }         
              }
              $('#myModal').modal('hide');


            }).error(function(data) {
                alert("ERROR AL CONSULTAR")
                $('#myModal').modal('hide');
            })
          }else if(obj['idCapas'].length>0){
            $.ajax({
              type: "POST",
              url: urlImport+"obtener-escuelas-capas",
              data: { _token : _token, idCapasReq:obj },
              beforeSend: function() {
                $('#myModal').modal('show'); 
              }
            }).done(function(dataR) {
              for (var x = 0; x < dataR.length; x++) {
                  if(dataR[x].length==0){
                    swal('¡Alerta!', 'No hay escuelas asignadas a esa capa', "info");
                    $("#M"+capa.value).removeAttr("checked");
                  }else if(markers.length==dataR[x].length){
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setVisible(true);
                    }
                  }else{
                    for (var i = 0; i < markers.length; i++) {
                        for(var j = 0; j < dataR[x].length; j++){
                          if(markers[i].id==dataR[x][j].id){
                            markers[i].setVisible(true);
                          }
                        } 
                      }
                    }
                  }           
              $('#myModal').modal('hide');    
            }).error(function(data) {
                alert("ERROR AL CONSULTAR")
                $('#myModal').modal('hide');
            })
          }
        }, 1500);
      }
 
      function buscarCapas(marker){
        var id = marker.id
        var _token = $('input[name="_token"]').val();
        $.ajax({
          type: "POST",
          url: urlImport+"obtener-detalles/"+id,
          data: { _token : _token },
        }).done(function(dataR) {
          $('#listaCapas'+id+'').html('');
          for (i in dataR){
            var fila = '<div><p> Nombre: '+dataR[i].nombre+'</p> <p> Descripcion: '+dataR[i].descripcion+'</p> <a class="btn btn-primary" href="'+urlImport+'/obtener-respuestas/'+dataR[i].id+'/'+id+' "><i class="far fa-file-excel"></i> </a></div>';
             $('#listaCapas'+id+'').append(fila);
          }
        })
        .error(function(data) {
          alert("ERROR AL CONSULTAR")
          //swal('¡Error!', 'No se pudo eliminar la capa', "error");
        })
      }
    </script>

@extends('layouts.blank')
@push('stylesheets')
     <style>

      #map {
        height: 70%;
        width: 100%;
      }
      .img_school{
        width: 35px;
      }

      .informacion-escuela{
        width: 250px;
      }
      /*
      .legend{
        background-color: rgba(97,75,75,.5);
        border-radius: 5px;
        margin-left: 5px;
      }
      */
      .legend-box{
        background-color: #FFFFFF;
        padding: 10px;
        border-radius: 5px;
        margin: 5px;
      }

      .controls{
        margin-right: 5px;
      }
      .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
      }

      /* Safari */
      @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
      }
    </style>

@endpush

@section('main_container')
    <!-- page content -->
    <script src="{{ asset("js/jquery.min.js") }}"></script>
    <div class="container">
        <div class="right_col" role="main">
         <div class="container">
             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" >
                    <h2>Mapa </h2>
                    <input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <div id="controls" class="controls">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="input-group">
                              <select class="form-control" id="tipoEscuela" name="tipoEscuela" onchange="controlFiltrado(this)">
                                <option selected disabled value="NoOpt">Contról</option>
                                  <option value="0">Todas las escuelas</option>
                                  <option value="PÚBLICO">PÚBLICO</option>
                                  <option value="PRIVADO">PRIVADO</option>
                                  <option value="DIF">DIF</option>
                                  <option value="DIF CDMX">DIF CDMX</option>
                                  <option value="GCDMX">GCDMX</option>
                                  <option value="COMUNITARIO">COMUNITARIO</option>
                                  <option value="FEDERAL">FEDERAL</option>
                                  <option value="PARTICULAR">PARTICULAR</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="input-group">
                              <select class="form-control" id="delegacion" name="delegacion" onchange="controlFiltrado(this)">
                                <option selected disabled value="NoOpt">Delegación</option>
                                 <option value="0">Todas las delegaciones</option>
                                 @foreach($delegaciones as $delegacion)
                                  <option value="{{$delegacion->id}}" >{{$delegacion->nombre}}</option>
                                 @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4 legend">
                            <div class="input-group legend-box">
                              <div class="checkbox "><label><input id="check_Todos"  type="checkbox" name="optCapas"value="0" > Seleccionar todas</label></div>
                              @foreach($modulos as $modulo)
                                <div class="checkbox "><label><input class="ck" type="checkbox" id="M{{$modulo->id}}" onclick="controlFiltrado(this)" name="optCapas"value="{{$modulo->id}}"> {{$modulo->nombre}}</label></div>
                              @endforeach

                            </div>
                          </div>
                        </div>
                      </div>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        </div></div>
    </div>
    </div>
    <!-- /page content -->
    <!-- footer content -->
  <footer>
    <div class="clearfix"></div>
  </footer>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvsrK2uGnuHA1EaKlqPNnFH074I-ZDTTs&callback=initMap"
    async defer></script>

                      <!-- Modal -->
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                        
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <h3>CARGANDO...</h3>
                            </div>
                            <div class="modal-body">
                              <div class="container">
                                <div class="row">
                                  <div class="col-md-4 col-md-offset-5">
                                    <div class="loader" id="loader"></div>
                                  </div>                           
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


  @endsection

@push ('scripts')
  <script src="{{ asset("js/global.js") }}"></script>
  <script>  
     $(document).ready(function(){
      $("#check_Todos").change(function(event){
        var contador=0
        $(".ck").prop('checked', $(this).prop("checked"));

        $("input:checkbox:checked").each(function(){
          contador++
        })

        if(contador>0){
          for (var i = 0; i < markers.length; i++){
            markers[i].setVisible(true);           
          }
        }else{
          for (var i = 0; i < markers.length; i++){
            markers[i].setVisible(false);           
          }
        }
      })
     })
   </script>
 @endpush
